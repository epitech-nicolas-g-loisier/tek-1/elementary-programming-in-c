/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** check if the function exist
*/

#include <stdlib.h>
#include "label.h"
#include "corewar.h"
#include "get_next_line.h"
#include "my.h"
#include "error_list.h"
#include "instruction.h"
#include "check_file.h"

int	skip_label(char *line, int idx, bool lib)
{
	if ((line[idx] != '\t' && line[idx] != ' ') &&
		check_label_instruction(line, &lib) == 0) {
		while (line[idx - 1] != ':') {
			idx ++;
		}
	}
	return (idx);
}

int	check_function(char *line, bool lib, int error)
{
	int idx = 0;
	int idx2 = 1;
	int taille = 0;

	if (error > 0 && error <= E_MAX_ID)
		return (error);
	idx = skip_label(line, idx, lib);
	while (line[idx] == '\t' || line[idx] == ' ')
		idx += 1;
	if (line[idx] =='.')
		return (0);
	while (idx2 <= 32) {
		taille = my_strlen(get_instruct_name(idx2 - 1));
		if (my_strncmp(&line[idx], get_instruct_name(idx2 - 1),
				taille) == 0)
			return (0);
		idx2 += 1;
	}
	return (E_INST);
}
