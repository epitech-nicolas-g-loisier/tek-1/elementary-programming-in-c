/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** return a tab of the shortest path
*/

#include <stdlib.h>
#include "graph.h"

static int get_link(graph_t *room)
{
	int i = 0;

	if (room == NULL || room->link == NULL)
		return (-1);
	else
		while (room->link[i] != NULL && room->link[i]->id != ID_ISR)
			i++;
	return (i);
}

static graph_t *get_end(graph_t *room)
{
	int i = 0;

	if (room == NULL || room->link == NULL)
		return (NULL);
	while (room->link[i] != NULL && room->link[i]->id != ID_END)
		i++;
	room = room->link[i];
	room->ant_id = 0;
	return (room);
}

static graph_t **restore_path_id(graph_t **path)
{
	int idx = 0;

	if (path == NULL)
		return (NULL);
	while (path[idx] != NULL) {
		if (path[idx]->id != ID_END)
			path[idx]->id = ID_ISR;
		idx++;
	}
	return (path);
}

graph_t	**get_path(graph_t** rooms, int size)
{
	graph_t **path = malloc(sizeof(graph_t*) * (size + 2));
	graph_t *tmp;
	int i = 0;

	tmp = rooms[0];
	if (path == NULL || rooms == NULL)
		return (NULL);
	for (int idx = 0; idx < size; idx++) {
		i = get_link(tmp);
		tmp = tmp->link[i];
		if (i == -1 || tmp == NULL)
			break;
		path[idx] = tmp;
		path[idx]->id = ID_ISU;
		path[idx]->ant_id = 0;
		path[(idx + 1)] = NULL;
	}
	path[size] = get_end(tmp);;
	path[(size + 1)] = NULL;
	return (restore_path_id(&path[0]));
}
