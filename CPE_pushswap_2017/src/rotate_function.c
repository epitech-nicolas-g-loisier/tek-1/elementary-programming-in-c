/*
** EPITECH PROJECT, 2017
** Push Swap
** File description:
** The first element become the last
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "pushswap.h"

void	rotate_list(list_t *list)
{
	add_to_list(list->previous, list->next->value);
	remove_to_list(list->next);
}

void	rotate_two_list(list_t *list_a, list_t *list_b)
{
	rotate_list(list_a);
	rotate_list(list_b);
	my_putstr("rr");
}

void	rotate_list_a(list_t *list_a)
{
	rotate_list(list_a);
	my_putstr("ra");
}

void	rotate_list_b(list_t *list_b)
{
	rotate_list(list_b);
	my_putstr("rb");
}
