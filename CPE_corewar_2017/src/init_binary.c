/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Adds the name and the description of the champion
*/

#include <unistd.h>
#include <stdlib.h>
#include "get_next_line.h"
#include "my.h"
#include "corewar.h"

static int	get_new_line(char **line, int fd)
{
	free(*line);
	*line = get_next_line(fd);
	if (!*line)
		return (84);
	return (0);
}

static int	write_desc(int src_fd, int dest_fd, char **line)
{
	char desc[2053] = "";
	int count = 0;
	int length = 0;

	while (line[0][count] == ' ' || line[0][count] == '\t')
		count++;
	count = count + 8;
	while (line[0][count] == ' ' || line[0][count] == '\t')
		count++;
	count++;
	length = count;
	while (line[0][length] != '"' && line[0][length] != '\0')
		length++;
	my_strncpy(&desc[0], &line[0][count], length - count);
	write(dest_fd, desc, 2052);
	return (get_new_line(line, src_fd));
}

static int	write_name(char **line, int fd)
{
	char name[133] = "";
	int count = 0;
	int length = 0;

	while (line[0][count] == ' ' || line[0][count] == '\t')
		count++;
	count = count + 5;
	while (line[0][count] == ' ' || line[0][count] == '\t')
		count++;
	count++;
	length = count;
	while (line[0][length] != '"' && line[0][length] != '\0')
		length++;
	my_strncpy(&name[0], &line[0][count], length - count);
	write(fd, name, 132);
	free(*line);
	return (0);
}

int	init_binary(int src_fd, int dest_fd, char **line, int size)
{
	const unsigned char magic_number[4] = {0, 234, 131, 243};

	*line = get_next_line(src_fd);
	while (*line && (line[0][0] == '#' || line[0][0] == '\0')) {
		free(*line);
		*line = get_next_line(src_fd);
	}
	if (!*line)
		return (84);
	write(dest_fd, magic_number, 4);
	write_name(line, dest_fd);
	write_big_endian(size, 4, dest_fd);
	*line = get_next_line(src_fd);
	while (*line && (line[0][0] == '#' || line[0][0] == '\0')) {
		free(*line);
		*line = get_next_line(src_fd);
	}
	if (*line)
		return (write_desc(src_fd, dest_fd, line));
	return (84);
}
