/*
** EPITECH PROJECT, 2017
** Bootstrap BSQ
** File description:
** write the first 500 bytes of a file
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void	fs_cat_500_bytes(char const *filepath)
{
	int	fd = 0;
	char	*buffer = malloc(sizeof(char) * 501);
	int	size = 0;

	fd = open(filepath, O_RDONLY);
	if (fd == -1)
		exit(84);
	size = read(fd, buffer, 500);
	if (size != -1)
		write(1, buffer, size);
	else
		exit(84);
}
