/*
** EPITECH PROJECT, 2018
** get_next_line.h
** File description:
** Defines the READ_SIZE macro
*/

#ifndef GNL_H_
#define GNL_H_

char	*get_next_line(int);

#endif /* GNL_H_ */

#ifndef READ_SIZE
#	define READ_SIZE 128
#endif
