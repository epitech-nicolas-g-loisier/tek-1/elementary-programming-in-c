/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** check if the start and the end have link
*/

#include <stdlib.h>
#include <unistd.h>
#include "graph.h"

static int check_end_link(graph_t *end)
{
	if (end == NULL || end->link == NULL) {
		write(2, "error: no link given to end\n", 28);
		return (84);
	} else if (end->link[0] == NULL) {
		write(2, "error: no link given to end\n", 28);
		return (84);
	} else
		return (0);
}

int	check_if_linked(graph_t **rooms)
{
	int idx = 0;
	int ret = 0;

	if (rooms == NULL || rooms[0] == NULL ||rooms[0]->id != ID_STA) {
		write(2, "error: no start\n", 16);
		return (84);
	} else if (rooms[0]->link == NULL || rooms[0]->link[0] == NULL) {
		write(2, "error: no link given to start\n", 30);
		return (84);
	}
	while (rooms[idx] != NULL) {
		if (rooms[idx]->id == ID_END)
			break;
		idx++;
	}
	ret = check_end_link(rooms[idx]);
	return (ret);
}
