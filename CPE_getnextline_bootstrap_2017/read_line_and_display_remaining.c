/*
** EPITECH PROJECT, 2018
** Bootstrap Get Next Line
** File description:
** return the first line and write the extra read character
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

char	*add_str(char *str, char *add, int stop, int n)
{
	char	*dest = malloc(sizeof(char) * (stop + n + 1));
	int	count = 0;
	int	counter = 0;

	while (str[count] != '\0'){
		dest[count] = str[count];
		count++;
	}
	while (counter < stop){
		dest[count] = add[counter];
		count++;
		counter++;
	}
	dest[count] = '\0';
	free(str);
	return (dest);
}

char	*read_line_and_display_remaining(int fd, int n)
{
	char	*buffer = malloc(sizeof(char) * n + 1);
	char	*dest = malloc(sizeof(char) * n + 1);
	int	size = n;
	int	count = 0;

	while (buffer[count] != '\n' && size == n){
		count = 0;
		size = read(fd, buffer, n);
		buffer[size] = '\0';
		while (buffer[count] != '\0' && buffer[count] != '\n'){
			count++;
		}
		dest = add_str(dest, buffer, count, n);
	}
	write(1, &buffer[count], (n - count));
	free(buffer);
	return (dest);
}
