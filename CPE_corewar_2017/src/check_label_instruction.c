/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** check if there is a label in a line
*/

#include <stdbool.h>
#include "error_list.h"

static bool	check_label_is_alpha(char label)
{
	if (label >= 'a' && label <= 'z')
		return (true);
	else if (label >= 'A' && label <= 'Z')
		return (true);
	else
		return (false);
}

static bool	check_label_is_num(char label)
{
	if (label >= '1' && label <= '9')
		return (true);
	else
		return (false);
}

bool	str_is_alphanum(char *str)
{
	int idx = 0;

	while (str[idx] != '\0') {
		if (str[idx] == '_' || check_label_is_num(str[idx]) == true)
			idx++;
		else if (check_label_is_alpha(str[idx]) == true)
			idx++;
		else {
			return (false);
		}
	}
	return (true);
}

static bool	check_label_char(char *str)
{
	int idx = 0;

	while (str[idx] != ':') {
		if (str[idx] == '_' || check_label_is_num(str[idx]) == true)
			idx++;
		else if (check_label_is_alpha(str[idx]) == true)
			idx++;
		else {
			return (false);
		}
	}
	return (true);
}

int	check_label_instruction(char *inst, bool *lab)
{
	int idx = 0;
	int tmp = 0;

	while (inst[tmp] == ' ' || inst[tmp] == '\t') {
		tmp++;
	}
	idx = tmp;
	while (inst[idx] != ' ' && inst[idx] != '\t' && inst[idx] != '\0') {
		idx++;
	}
	if (idx != 0 && inst[(idx - 1)] == ':') {
		if (check_label_char(&inst[tmp]) == true) {
			*lab = true;
			return (0);
		} else
			return (E_LNAME);
	} else
		return (0);
}
