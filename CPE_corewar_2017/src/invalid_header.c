/*
** EPITECH PROJECT, 2018
** corewar assembler
** File description:
** functions which check invalid name and comment
*/

#include "check_file.h"
#include "error_list.h"
#include "my.h"

int	check_multi_name(char *line, bool name, int error)
{
	int idx = 0;

	if (error > 0 && error <= E_MAX_ID)
		return (error);
	while (line[idx] == '\t' || line[idx] == ' ')
		idx ++;
	if (name == true && my_strncmp(&line[idx], ".name", 5) == 0)
		return (E_MNAME);
	else
		return (0);
}

int	invalid_header_instruction(char *line)
{
	int idx = 0;

	while (line[idx] == '\t' || line[idx] == ' ') {
		idx ++;
	}
	if (line[idx] != '.')
		return (0);
	if (my_strncmp(&line[idx], ".name", 5) == 0 ||
	my_strncmp(&line[idx], ".comment", 8) == 0)
		return (0);
	return (E_INST);
}

int	invalid_header(char *line, int error)
{
	static bool name = false;

	error = check_multi_name(line, name, error);
	if (error > 0 && error <= E_MAX_ID) {
		if (error == E_MNAME)
			name = false;
		return (error);
	}
	if (line[0] == '#' || line[0] == '\0')
		return (0);
	else {
		error = check_name_error(line);
		if (error == 0)
			error = check_cmnt_error(line, name);
	}
	name = true;
	return (error);
}
