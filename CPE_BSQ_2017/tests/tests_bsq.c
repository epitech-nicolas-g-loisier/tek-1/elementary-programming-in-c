/*
** EPITECH PROJECT, 2017
** BSQ
** File description:
** Unit tests
*/

#include <criterion/criterion.h>
#include "bsq.h"
#include "my.h"

Test(bsq, find_square)
{
	char	**map = malloc(sizeof(char *) * 5);
	square_t *square = malloc(sizeof(square_t));

	square->x = 0;
	square->y = 0;
	square->size = 0;

	map[0] = my_strdup(".....o.\n");
	map[1] = my_strdup(".....o.\n");
	map[2] = my_strdup(".....o.\n");
	map[3] = my_strdup(".....o.\n");
	map[4] = NULL;
	find_square(0, 0, map, square);
	cr_expect_eq(square->size, 4);
}

Test(bsq, find_square_empty)
{
	char	**map = malloc(sizeof(char *) * 6 );
	square_t *square = malloc(sizeof(square_t));

	square->x = 0;
	square->y = 0;
	square->size = 0;

	map[0] = my_strdup(".......\n");
	map[1] = my_strdup(".......\n");
	map[2] = my_strdup(".......\n");
	map[3] = my_strdup(".......\n");
	map[4] = my_strdup(".......\n");
	map[5] = NULL;
	find_square(0, 0, map, square);
	cr_expect_eq(square->size, 5);
}
