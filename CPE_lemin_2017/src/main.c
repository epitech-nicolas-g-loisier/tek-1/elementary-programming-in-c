/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** main for the project
*/

#include <stdlib.h>
#include <unistd.h>
#include "lemin.h"
#include "graph.h"

void	do_lem_in(graph_t ***paths, int *paths_size, int nb_ants)
{
	int count = nb_ants;
	int status = 0;

	for (int i = 0; paths[i]; i++) {
		for (int j = 0; paths[i][j]; j++)
			if (paths[i][j]->id == ID_ISU)
				paths[i][j]->id = ID_NOR;
	}
	write(1, "#moves\n", 7);
	while (count > 0 || status != 0) {
		print_ant(paths, paths_size, nb_ants);
		status = 0;
		for (int i = 0; paths[i] != NULL; i++)
			status += move_ants(paths[i]);
		if (count > 0)
			status++;
		add_ants(paths, paths_size, &count);
	}
	put_ant(0, NULL);
}

int	main(void)
{
	int nb_ants = 0;
	int *paths_size = NULL;
	graph_t **rooms = get_file_data(&nb_ants);
	graph_t ***paths = NULL;

	if (!rooms || check_if_linked(rooms) == 84)
		return (84);
	paths = find_max_path(rooms);
	paths_size = prune_path(paths, nb_ants);
	if (!paths_size || !paths || !paths[0])
		return (84);
	do_lem_in(paths, paths_size, nb_ants);
	free(paths_size);
	free_path(paths);
	free_graph(rooms);
	return (0);
}
