/*
** EPITECH PROJECT, 2018
** Matchsticks
** File description:
** remove matches from the map
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "matchstick.h"

char	*remove_matches(int max, int lines, int idx)
{
	char *map = malloc(sizeof(char) * (lines * 2 + 3));
	int count = 1;
	int matches = 0;

	if (map == NULL)
		return (NULL);
	map[0] = '*';
	while (count < (lines * 2)) {
		if (count > (lines - idx) && matches < max) {
			map[count] = '|';
			matches++;
		}
		else
			map[count] = ' ';
		count++;
	}
	map[count] = '*';
	map[(count + 1)] = '\n';
	map[(count + 2)] = '\0';
	return (map);
}
