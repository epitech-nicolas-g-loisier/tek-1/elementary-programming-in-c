/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** create a link between 2 rooms given in buffer
*/

#include <stdlib.h>
#include <unistd.h>
#include "graph.h"
#include "my.h"
#include "lemin.h"

char	**get_link_name(char *str)
{
	int idx = 0;
	char **name = malloc(sizeof(char*) * 2);

	while (str[idx] == ' ' || str[idx] == '\t')
		idx++;
	name[0] = get_room_name(str, &idx);
	if (name[0] == NULL)
		return (NULL);
	else if (str[idx] != '-') {
		free(name[0]);
		return (NULL);
	}
	idx++;
	name[1] = get_room_name(str, &idx);
	if (name[1] == NULL) {
		free(name[0]);
		return (NULL);
	} else if (check_end(&str[idx]) != 0)
		return (NULL);
	return (name);
}

static void free_name(char **name)
{
	if (name != NULL) {
		if (name[0] != NULL)
			free(name[0]);
		if (name[1] != NULL)
			free(name[1]);
		free(name);
	}
}

int	*get_link_pos(char **name, graph_t **room)
{
	int *pos = malloc(sizeof(int*) * 2);

	if (name == NULL || room == NULL || pos == NULL)
		return (NULL);
	else {
		pos[0] = -1;
		pos[1] = -1;
	}
	for (int idx = 0; room[idx] != NULL; idx++)
		if (my_strcmp(room[idx]->name, name[0]) == 0) {
			pos[0] = idx;
			break;
		}
	for (int idx = 0; room[idx] != NULL; idx++)
		if (my_strcmp(room[idx]->name, name[1]) == 0) {
			pos[1] = idx;
			break;
		}
	free_name(name);
	return (pos);
}

int	get_room_link(char *str, graph_t **rooms, int *line)
{
	static int first = 0;
	int *pos;
	int ret = 0;

	if (first == 0) {
		write(1, "#tunnels\n", 9);
		first = 1;
	}
	pos = get_link_pos(get_link_name(str), rooms);
	if (pos == NULL)
		return (84);
	else if (pos[0] == -1 || pos[1] == -1)
		return (84);
	ret = create_link(rooms[pos[0]], rooms[pos[1]]);
	*line = 2;
	write(1, str, my_strlen(str));
	write(1, "\n", 1);
	free(pos);
	return (ret);
}
