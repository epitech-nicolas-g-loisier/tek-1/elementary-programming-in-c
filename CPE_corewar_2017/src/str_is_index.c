/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Checks if the given param is an index
*/

#include <stdbool.h>
#include "corewar.h"

bool	str_is_index(char *param, int *progress)
{
	int tmp = 0;

	if (!param)
		return (false);
	if (str_is_indirect(param, &tmp) || str_is_direct(param, &tmp)) {
		*progress = *progress + 2;
		return (true);
	}
	return (false);
}
