/*
** EPITECH PROJECT, 2018
** Get Next Line
** File description:
** Get Next Line
*/

#ifndef READ_SIZE
#define READ_SIZE (4096)
#endif /* READ_SIZE */

#ifndef GETNEXTLINE_H_
#define GETNEXTLINE_H_

char	*get_next_line(int);

#endif /* GETNEXTLINE_H_ */
