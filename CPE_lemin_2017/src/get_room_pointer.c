/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** Get pointer for a room
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "graph.h"
#include "lemin.h"

graph_t	**cp_tab(graph_t **src, int max)
{
	int idx = 0;
	graph_t **dest = malloc(sizeof(graph_t*) * (max + 3));

	if (dest == NULL)
		return (NULL);
	while (src[idx] != NULL) {
		dest[idx] = src[idx];
		idx++;
	}
	dest[idx] = NULL;
	dest[(idx + 1)] = NULL;
	free(src);
	return (dest);
}

graph_t	**realloc_rooms(graph_t **tab)
{
	graph_t **new_tab;
	int idx = 0;

	if (tab == NULL) {
		new_tab = malloc(sizeof(graph_t*) * 2);
		if (new_tab == NULL)
			return (NULL);
		new_tab[0] = NULL;
		new_tab[1] = NULL;
	} else {
		while (tab[idx] != NULL) {
			idx++;
		}
		new_tab = cp_tab(tab, idx);
		new_tab[idx] = NULL;
		new_tab[(idx + 1)] = NULL;
	}
	return (new_tab);
}

graph_t	**get_room_pointer(char *str, int id, graph_t **rooms)
{
	graph_t *new_room = malloc(sizeof(graph_t));
	graph_t **new_rooms;
	int idx = 0;

	new_rooms = realloc_rooms(rooms);
	if (new_rooms == NULL || new_room == NULL)
		return (NULL);
	while (str[idx] == ' ' || str[idx] == '\t')
		idx++;
	new_room->name = get_room_name(str, &idx);
	new_room->id = id;
	new_room->link = NULL;
	idx = 0;
	while (new_rooms[idx] != NULL) {
		idx++;
	}
	new_rooms[idx] = new_room;
	new_rooms[(idx + 1)] = NULL;
	write(1, str, my_strlen(str));
	write(1, "\n", 1);
	return (new_rooms);
}
