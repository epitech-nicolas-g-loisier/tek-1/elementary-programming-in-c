/*
** EPITECH PROJECT, 2018
** Need4stek
** File description:
** Need4stek prototypes
*/

#ifndef N4S_H_
#define N4S_H_

double	get_float(char *);
double  calc_next_instruction(char *, int *, double *);
int *line_parser(char *, int);
void	put_float(double);
void	radar_average(char *, int *, double[2]);
void	get_answer(char **, int**, int);

#endif /* N4S_H_ */

#ifndef ABS
#define ABS(x) (x >= 0 ? x : x * -1)
#endif /* ABS */
