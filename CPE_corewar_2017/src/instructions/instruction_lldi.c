/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** function for lldi instruction
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"
#include "label.h"

int	instruction_lldi(int fd, char *str, label_t *label, int *pos)
{
	int instruct = LLDI;
	char **param;
	int nb = 0;
	int byte = 2;
	int tmp = 0;

	write(fd, &instruct, 1);
	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	nb = calc_description(param);
	write(fd, &nb, 1);
	tmp = direct_to_indirect(param[1]);
	byte += write_parameter(&param[1][tmp], fd, label, *pos);
	tmp = direct_to_indirect(param[2]);
	byte += write_parameter(&param[2][tmp], fd, label, *pos);
	byte += write_parameter(param[3], fd, label, *pos);
	free_parameter(param);
	*pos += byte;
	return (0);
}
