/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Ignores the indentation before the instruction
*/

int	ignore_indent(char *line)
{
	int i = 0;

	if (!line)
		return (0);
	while (line[i] == ' ' || line[i] == '\t')
		i++;
	return (i);
}
