/*
** EPITECH PROJECT, 2017
** Push Swap
** File description:
** Swap functions for Push Swap
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "pushswap.h"

void	swap_list(list_t *list)
{
	int	tmp = list->next->value;	
	list->next->value = list->next->next->value;
	list->next->next->value = tmp;
}

void	swap_two_list(list_t *list_a, list_t *list_b)
{
	swap_list(list_a);
	swap_list(list_b);
	my_putstr("sc");
}

void	swap_list_a(list_t *list_a)
{
	swap_list(list_a);
	my_putstr("sa");
}

void	swap_list_b(list_t *list_b)
{
	swap_list(list_b);
	my_putstr("sb");
}
