/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** function for the lld instruction
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "corewar.h"
#include "label.h"
#include "instruction.h"

int	instruction_lld(int fd, char *str, label_t *label, int *pos)
{
	int instruct = LLD;
	char **param;
	int nb = 0;
	int byte = 2;

	write(fd, &instruct, 1);
	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	nb = calc_description(param);
	write(fd, &nb, 1);
	byte += write_parameter(param[1], fd, label, *pos);
	byte += write_parameter(param[2], fd, label, *pos);
	free_parameter(param);
	*pos += byte;
	return (0);
}
