/*
** EPITECH PROJECT, 2017
** my_putchar
** File description:
** write letter
*/

#include <unistd.h>

void	my_putchar(void *c)
{
	write (1, &c, 1);
}
