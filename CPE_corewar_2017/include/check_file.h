/*
** EPITECH PROJECT, 2018
** corewar assembler
** File description:
** Prototypes of the error_checker function used in the project
*/

#ifndef __CHECK_FILE__
#define __CHECK_FILE__

#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "label.h"

int	check_file_content(char *pathname, label_t *label, int *pos);

void	error_message(char *pathname, int nb_line, char *err_type);

int     error_fptr(char *pathname, int nb_linr, int error);

int	invalid_instruction(char *line, int error);

int	error_checker(char *line, label_t *label, int *pos, int error);

int	invalid_header(char *line, int error);

int	invalid_header_instruction(char *line);

int	check_name_error(char *name);

int	check_cmnt_error(char *comment, bool name_is_defined);

int	check_label_instruction(char *inst, bool *lab);

int	check_label(char *line, label_t *label, int pos, int error);

int	check_function(char *line, bool lab, int error);

int	check_arguments(char *line, int *pos, int error);

char	*get_instruct_name(int index);

int	check_add(char *line, int *pos);

int	check_aff(char *line, int *pos);

int	check_and(char *line, int *pos);

int	check_ld(char *line, int *pos);

int	check_ldi(char *line, int *pos);

int	check_live(char *line, int *pos);

int	check_st(char *line, int *pos);

int	check_sti(char *line, int *pos);

int	check_zjmp(char *line, int *pos);

#endif /* __ERROR_GESTION__ */
