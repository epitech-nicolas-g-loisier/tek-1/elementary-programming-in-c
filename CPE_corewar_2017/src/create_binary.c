/*
** EPITECH PROJECT, 2018
** Corewar
** File description:
** Creates the file to be written on
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "my.h"

static char	*get_filename(char const *src_name)
{
	char *filename = NULL;
	int count = 0;

	count = my_strlen(src_name);
	if (count >= 2 && my_strcmp(&src_name[count - 2], ".s") == 0)
		count = count - 2;
	filename = malloc(sizeof(char) * (count + 5));
	if (filename == NULL)
		return (NULL);
	for (int i = 0; i < count; i++)
		filename[i] = src_name[i];
	filename[count] = '.';
	filename[count + 1] = 'c';
	filename[count + 2] = 'o';
	filename[count + 3] = 'r';
	filename[count + 4] = '\0';
	return (filename);
}

int	create_binary(char const *src_name)
{
	char *filename = get_filename(src_name);
	int fd = -1;

	if (filename == NULL) {
		write(2, "asm: memory full\n", 17);
		return (-1);
	}
	fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC,
		S_IRWXU | S_IRWXG | S_IRWXO);
	free(filename);
	if (fd == -1)
		write(2, "asm: could not create file\n", 27);
	return (fd);
}
