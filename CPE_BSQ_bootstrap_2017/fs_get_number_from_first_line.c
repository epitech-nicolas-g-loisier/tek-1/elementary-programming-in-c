/*
** EPITECH PROJECT, 2017
** Bootstrap BSQ
** File description:
** retrieve and return the positive number written on the first line
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int	fs_get_number_from_first_line(char const* filepath)
{
	int	fd = 0;
	char	*buffer = malloc(sizeof(char) * 2);
	int	number = 0;
	int	size = 1;

	fd = open(filepath, O_RDONLY);
	if (fd == -1)
		return -1;
	while (buffer[0] != '\n' && size != 0){
		size = read(fd, buffer, 1);
		if (size == -1 || buffer[0] < '1' || buffer[0] > '9')
			return -1;
		number = number * 10;
		number += buffer[0] - '0';
	}
	number = number / 10;
	return number;
}
