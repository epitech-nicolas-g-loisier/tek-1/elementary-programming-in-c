/*
** EPITECH PROJECT, 2017
** Push Swap
** File description:
** ordonate an list
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "pushswap.h"

int	check_algo(list_t *list_a)
{
	list_t	*tmp_list = list_a->next->next;
	int	check = 0;

	while (tmp_list != list_a){
		if (tmp_list->previous->value > tmp_list->value)
			check++;
		tmp_list = tmp_list->next;
	}
	if (check != 0){
		return (1);
	}
	return (0);
}

void	sorting_algo_bis(list_t *list_a, list_t *list_b)
{
	list_t	*tmp_list = list_b->next;
	list_t	*tp_list = list_b->next->next;

	if (list_a->next->value > list_a->next->next->value){
		swap_list_a(list_a);
	} else if (tmp_list->value < tp_list->value && tp_list != list_b){
		swap_list_a(list_b);
	} else if (list_a->next->value >= list_a->previous->value){
		reverse_rotate_list_a(list_a);
	} else if (list_b->next->value < list_b->previous->value){
		reverse_rotate_list_b(list_b);
	}else
		if (check_algo(list_a) == 1){
			switch_lista_to_listb(list_a, list_b);
		}
}

void	sorting_algo(list_t *list_a, list_t *list_b)
{
	list_t	*tmp_list = list_b->next;
	list_t	*tp_list = list_b->next->next;

	if (list_a->next->value > list_a->next->next->value){
		my_putchar(' ');
		swap_list_a(list_a);
	} else if (tmp_list->value < tp_list->value && tp_list != list_b){
		my_putchar(' ');
		swap_list_a(list_b);
	} if (list_a->next->value >= list_a->previous->value){
		my_putchar(' ');
		reverse_rotate_list_a(list_a);
	} else if (list_b->next->value < list_b->previous->value){
		my_putchar(' ');
		reverse_rotate_list_b(list_b);
	}else
		if (check_algo(list_a) == 1){
			my_putchar(' ');
			switch_lista_to_listb(list_a, list_b);
		}
}

int	main(int argc, char **argv)
{
	int	i = argc - 1;
	int	valeur;
	list_t	*list_a = creat_root();
	list_t	*list_b = creat_root();

	if (argc < 2 || list_a == NULL || list_b == NULL)
		return (84);
	while (i > 0){
		valeur = my_getnbr(argv[i]);
		add_to_list(list_a, valeur);
		i--;
	}
	if (check_algo(list_a) == 1)
		sorting_algo_bis(list_a, list_b);
	while (check_algo(list_a) == 1 || list_b->next != list_b){
		while (check_algo(list_a) == 1){
			sorting_algo(list_a, list_b);
		} while (list_b->next != list_b){
			my_putchar(' ');
			switch_listb_to_lista(list_b, list_a);
		}
	}
	my_putchar('\n');
	return (0);
}
