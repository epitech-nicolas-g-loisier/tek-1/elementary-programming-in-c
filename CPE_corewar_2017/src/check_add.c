/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** check arguement given to add and sub
*/

#include <stdio.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"
#include "error_list.h"

int check_add(char *line, int *pos)
{
	char **stock = NULL;
	int i = 1;
	const int arg = 3;

	*pos = *pos + 2;
	stock = parameter_parser(line);
	if (stock == NULL)
		return (E_MEM);
	if (check_length(arg, stock) != 0) {
		return (check_length(arg, stock));
	}
	while (i <= 3) {
		if (str_is_register(stock[i], pos) != 0)
			return (E_INARG);
		i +=1;
	}
	free_parameter(stock);
	return (0);
}
