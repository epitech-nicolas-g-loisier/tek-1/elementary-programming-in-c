/*
** EPITECH PROJECT, 2017
** BSQ
** File description:
** main for BSQ
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <bsq.h>
#include <my.h>

int	main(int argc, char **argv)
{
	if (argc != 2){
		write(2, "invalid usage, try: ./bsq <filepath>\n", 38);
		return 84;
	} else if (open(argv[1], O_RDONLY) == -1){
		write(2, "invalid file\n", 13);
		return 84;
	}
	find_largest_square(argv[1]);
	return 0;
}
