/*
** EPITECH PROJECT, 2018
** corewar assembler
** File description:
** functions which check if functions arguments are valid
*/

#include "my.h"
#include "error_list.h"
#include "check_file.h"
#include "corewar.h"
#include <stdio.h>

bool	empty_line(char *str)
{
	int idx = 0;

	while (str[idx] != '\0') {
		if (str[idx] != ' ' && str[idx] != '\t')
			return (false);
		idx++;
	}
	return (true);
}

void fptr_tab_init(int (**fptr_arguments)(char *line, int *pos))
{
	fptr_arguments[0] = &check_add;
	fptr_arguments[1] = &check_aff;
	fptr_arguments[2] = &check_and;
	fptr_arguments[3] = &check_ld;
	fptr_arguments[4] = &check_ldi;
	fptr_arguments[5] = &check_live;
	fptr_arguments[6] = &check_st;
	fptr_arguments[7] = &check_sti;
	fptr_arguments[8] = &check_zjmp;
}

int	check_arguments(char *line, int *pos, int error)
{
	int (*fptr_arguments[9])(char *line, int *pos);
	int fcnt_value = -1;

	if (error > 0 && error <= E_MAX_ID) {
		return (error);
	}
	fcnt_value = check_nb_function(line);
	if (fcnt_value == -1)
		return (0);
	else {
		fptr_tab_init(fptr_arguments);
		if (empty_line(line) == true)
			return (error);
		error = fptr_arguments[fcnt_value](line, pos);
		return (error);
	}
}
