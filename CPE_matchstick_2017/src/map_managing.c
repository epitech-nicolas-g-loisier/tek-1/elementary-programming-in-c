/*
** EPITECH PROJECT, 2018
** Matchstick
** File description:
** manage game map
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "matchstick.h"

char	*full_line(int lines, int idx)
{
	char *map = malloc(sizeof(char) * (lines * 2 + 3));
	int count = 1;

	map[0] = '*';
	while (count < (lines * 2)) {
		if (count > (lines - idx) && count < (lines + idx))
			map[count] = '|';
		else
			map[count] = ' ';
		count++;
	}
	map[count] = '*';
	map[(count + 1)] = '\n';
	map[(count + 2)] = '\0';
	return (map);
}

char	*extrem_line(int lines)
{
	char *map = malloc(sizeof(char) * (lines * 2 + 3));
	int count = 0;

	while (count < (lines * 2 + 1)) {
		map[count] = '*';
		count++;
	}
	map[count] = '\n';
	map[(count + 1)] = '\0';
	return (map);
}

char	**create_map(int lines)
{
	char **map = malloc(sizeof(char*) * (lines + 3));
	int idx = 0;

	map[idx] = extrem_line(lines);
	idx++;
	while (idx < (lines + 1)) {
		map[idx] = full_line(lines, idx);
		idx++;
	}
	map[idx] = extrem_line(lines);
	idx++;
	map[idx] = NULL;
	return (map);
}

void	display_map(char **map)
{
	int idx = 0;

	while (map[idx]) {
		write(1, map[idx], my_strlen(map[idx]));
		idx++;
	}

}

void	free_map(char **map)
{
	int idx = 0;

	while (map[idx]) {
		free(map[idx]);
		idx++;
	}
	free(map);
}
