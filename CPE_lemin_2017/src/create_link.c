/*
** EPITECH PROJECT, 2018
** lemin
** File description:
** Creates a link between two rooms
*/

#include <stdlib.h>
#include "graph.h"

static void	realloc_link_array(graph_t *room, graph_t *new_link)
{
	int size = 0;
	graph_t **link_array = NULL;

	while (room->link != NULL && room->link[size] != NULL)
		size++;
	link_array = malloc(sizeof(graph_t *) * (size + 2));
	if (link_array) {
		for (int i = 0; i < size; i++)
			link_array[i] = room->link[i];
		link_array[size] = new_link;
		link_array[size + 1] = NULL;
		free(room->link);
		room->link = link_array;
	} else {
		free(room->link);
		room->link = NULL;
	}
}

int	create_link(graph_t *room1, graph_t *room2)
{
	realloc_link_array(room1, room2);
	if (room1->link == NULL)
		return (84);
	realloc_link_array(room2, room1);
	if (room2->link == NULL)
		return (84);
	return (0);
}
