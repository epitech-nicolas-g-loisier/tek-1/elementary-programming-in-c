/*
** EPITECH PROJECT, 2018
** need4stek
** File description:
** main function of the project
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include "my.h"
#include "n4s.h"
#include "get_next_line.h"

static int	stop_simulation(char **line, int **parsing)
{
	if (!*line || !*parsing)
		return (84);
	free(*line);
	free(*parsing);
	write(1, "STOP_SIMULATION\n", 16);
	*line = get_next_line(0);
	if (!*line)
		return (84);
	free(*line);
	return (0);
}

static bool	init_simulation(void)
{
	char *line = NULL;

	write(1, "START_SIMULATION\n", 17);
	line = get_next_line(0);
	if (!line || my_strcmp(line, "1:OK:No errors so far:No further info"))
		return (false);
	free(line);
	write(1, "CAR_FORWARD: 1\n", 15);
	line = get_next_line(0);
	if (!line)
		return (false);
	free(line);
	return (true);
}

static void	input_instrucion(char *instruction, double value)
{
	my_putstr(instruction);
	put_float(value);
}

static bool	check_answer(char **line, int **parsing, int answer_type)
{
	char *temp_line = NULL;
	int *temp_pars = NULL;

	get_answer(line, parsing, answer_type);
	if (*line == NULL || *parsing == NULL)
		return (false);
	temp_line = *line;
	temp_pars = *parsing;
	if (!my_strncmp(&temp_line[temp_pars[*temp_pars]], "Track Cleared", 13))
		return (false);
	return (true);
}

int	main(void)
{
	double speed = 0;
	double rotation = 0;
	char *line = NULL;
	int *parsing = NULL;

	if (!init_simulation())
		return (84);
	do {
		write(1, "GET_INFO_LIDAR\n", 15);
		if (!check_answer(&line, &parsing, 1))
			break;
		speed = calc_next_instruction(line, parsing, &rotation);
		input_instrucion("CAR_FORWARD:", speed);
		if (!check_answer(&line, &parsing, 0))
			break;
		input_instrucion("WHEELS_DIR:", rotation);
	} while (check_answer(&line, &parsing, 0));
	return (stop_simulation(&line, &parsing));
}