/*
** EPITECH PROJECT, 2017
** my_str_isprintable
** File description:
** Searches if string is printable
*/

int	my_str_isprintable(char const *str)
{
	int argc = 0;

	while (str[argc] != '\0') {
		if (str[argc] < 127 && str[argc] > 31)
			argc = argc + 1;
		else
			return (0);
	}
	return (1);
}
