/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Functions returning pointers to the instructions
*/

#include <stdbool.h>
#include <stdlib.h>
#include "instruction.h"
#include "label.h"
#include "corewar.h"

int	(*get_long_function_pointer(int index))(int, char *, label_t *, int *)
{
	static int (*ptr[13])(int, char *, label_t *, int *) = {
		&instruction_live,
		&instruction_ld,
		&instruction_st,
		&instruction_and,
		&instruction_or,
		&instruction_xor,
		&instruction_zjmp,
		&instruction_ldi,
		&instruction_sti,
		&instruction_fork,
		&instruction_lld,
		&instruction_lldi,
		&instruction_lfork};

	return (ptr[index]);
}

int	(*get_short_function_pointer(int index))(int, char *, int *)
{
	static int (*ptr[3])(int, char *, int *) = {&instruction_add,
							&instruction_sub,
							&instruction_aff};

	return (ptr[index]);
}

bool	is_short_function_index(int *index)
{
	if (*index == ADD || *index == SUB || *index == AFF) {
		*index = *index - ADD;
		if (*index > 1)
			*index = 2;
		return (true);
	}
	*index = *index - 1;
	if (*index > ADD)
		*index = *index - 2;
	return (false);
}
