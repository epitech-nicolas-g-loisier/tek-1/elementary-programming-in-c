/*
** EPITECH PROJECT, 2017
** Push Swap
** File description:
** The last element become the first
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "pushswap.h"

void	reverse_rotate_list(list_t *list)
{
	add_to_list(list, list->previous->value);
	remove_to_list(list->previous);
}

void	reverse_rotate_two_list(list_t *list_a, list_t *list_b)
{
	reverse_rotate_list(list_a);
	reverse_rotate_list(list_b);
	my_putstr("rrr");
}

void	reverse_rotate_list_a(list_t *list_a)
{
	reverse_rotate_list(list_a);
	my_putstr("rra");
}

void	reverse_rotate_list_b(list_t *list_b)
{
	reverse_rotate_list(list_b);
	my_putstr("rrb");
}
