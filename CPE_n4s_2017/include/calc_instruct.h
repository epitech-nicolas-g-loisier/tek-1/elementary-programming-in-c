/*
** EPITECH PROJECT, 2018
** Need4steck
** File description:
** global value and MACRO for N4S
*/

#include <stdio.h>

#ifndef GLOBAL_VALUE
#define GLOBAL_VALUE
const double MAX_ACC = 0.09;
const double SPEED_RATE = 6750.0;
const double BRAKE_DIST = 1600.0;
const double MAX_RAW_ROT = 0.5;
const double BRAKE_SPEED = 0.575;
#endif /* GLOBAL_VALUE */

#ifndef ROTATE_MODIF
#define ROTATE_MODIF(x) (MAX_RAW_ROT - x / BRAKE_DIST)
#endif /* ROTATE_MODIF */

#ifndef ROTATE_CALC
#define ROTATE_CALC(x) (3.6 * pow(x, 3) - 3.4 * pow(x, 2) + 0.45)
#endif /* ROTATE_CALC */
