/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** function for the aff instruction
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"

int	instruction_aff(int fd, char *str, int *pos)
{
	int instruct = AFF;
	char **param = NULL;
	int nb = 0;

	write(fd, &instruct, 1);
	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	nb = calc_description(param);
	write(fd, &nb, 1);
	nb = my_getnbr(param[1]);
	write(fd, &nb, 1);
	free_parameter(param);
	*pos += 2;
	return (0);
}
