/*
** EPITECH PROJECT, 2018
** n4s
** File description:
** Parses the info in the line given as arg
*/

#include <stdlib.h>
#include "my.h"

int	*line_parser(char *line, int answer_id)
{
	int count = 0;
	int idx = 1;
	const int size_arr[4] = {4, 36, 5, 6};
	int *pos = NULL;

	pos = malloc(sizeof(int) * (size_arr[answer_id] + 1));
	if (!pos)
		return (NULL);
	pos[0] = size_arr[answer_id] - 1;
	while (line[count] != '\0' && idx <= pos[0]) {
		count++;
		if (line[count] == ':') {
			line[count] = '\0';
			count++;
			pos[idx] = count;
			idx++;
		}
	}
	return (pos);
}
