/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** get the id of a room
*/

#include <unistd.h>
#include "graph.h"
#include "my.h"

static int get_start(int command[2], int *last)
{
	if (command[0] != 0 && *last != 1)
		return (ID_NOR);
	else if (*last == 0)
		write(1, "##start\n", 8);
	command[0] = 1;
	*last = 1;
	return (ID_STA);
}

static int get_end(int command[2], int *last)
{
	if (command[1] != 0 && *last != 2)
		return (ID_NOR);
	else if (*last != 2)
		write(1, "##end\n", 6);
	command[1] = 1;
	*last = 2;
	return (ID_END);
}

int	get_id(char *str)
{
	static int command[3] = {0, 0};
	static int last = 0;
	int ret = 0;

	if (my_strcmp("##start", str) == 0) {
		ret = get_start(&command[0], &last);
	} else if (my_strcmp("##end", str) == 0) {
		ret = get_end(&command[0], &last);
	} else {
		last = 0;
		ret = ID_NOR;
	}
	return (ret);
}
