/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** return the number of the function
*/

#include "my.h"
#include "corewar.h"

static int	check_label_name(char *str, int max)
{
	int idx = 0;
	int ret[16] = {5, 3, 6, 0, 0, 2, 2, 2, 8, 4, 7, 8, 3, 4, 8, 1};

	while (idx < 32) {
		if (my_strncmp(str, get_instruct_name(idx), max) == 0)
			break;
		else
			idx++;
	}
	if (idx == 32)
		return (-1);
	else if (idx > 15)
		idx -= 16;
	return (ret[idx]);
}

static int	find_first_word(char *str, int *tmp)
{
	int idx = *tmp;

	while (str[idx] == ' ' || str[idx] == '\t') {
		idx++;
	}
	*tmp = idx;
	while (str[idx] != ' ' && str[idx] != '\t' && str[idx] != '\0') {
		idx++;
	}
	return (idx);
}

int	check_nb_function(char *str)
{
	int idx = 0;
	int tmp = 0;
	int ret = -1;

	idx = find_first_word(str, &tmp);
	if (str[(idx - 1)] != ':') {
		ret = check_label_name(&str[tmp], (idx - tmp));
	}
	else if (str[idx] != '\0'){
		tmp = idx;
		idx = find_first_word(str, &tmp);
		ret = check_label_name(&str[tmp], (idx - tmp));
	}
	return (ret);
}
