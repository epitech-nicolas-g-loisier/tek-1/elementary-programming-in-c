/*
** EPITECH PROJECT, 2017
** BSQ
** File description:
** get data from file
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "bsq.h"

int	get_nbr_from_first_line(char const* filepath)
{
	int	fd = 0;
	char	*buffer = malloc(sizeof(char) * 2);
	int	number = 0;
	int	size = 1;

	buffer[0] = 0;
	fd = open(filepath, O_RDONLY);
	while (buffer[0] != '\n'){
		read(fd, buffer, size);
		if (buffer[0] == '\n')
			break;
		number = number * 10;
		number += buffer[0] - '0';
		if (size == -1 || buffer[0] < '0' || buffer[0] > '9')
			exit(84);
	}
	close(fd);
	free(buffer);
	return number;
}

int	get_length_of_first_line(char const *filepath)
{
	int	fd = 0;
	char	*buffer = malloc(sizeof(char) * 2);
	int	count = 0;
	int	size = 1;

	buffer[0] = 0;
	fd = open(filepath, O_RDONLY);
	while (buffer[0] != '\n' && size != 0){
		size = read(fd, buffer, 1);
	}
	buffer[0] = 0;
	while (buffer[0] != '\n' && size != 0){
		size = read(fd, buffer, 1);
		count++;
	}
	close(fd);
	free(buffer);
	return count;
}

char	**take_map(int nb_line, int length_line, char const* path)
{
	int	fd = 0;
	int	line_count = 0;
	int	size = length_line;
	char	**map = malloc(sizeof(char*) * (nb_line + 2));
	char	*buffer = malloc(sizeof(char) * (size + 1));

	fd = open(path, O_RDONLY);
	read(fd, buffer, (count_digit(nb_line) + 1));
	while (line_count < nb_line){
		read(fd, buffer, size);
		buffer[size] = 0;
		if (buffer[size-1] != '\n')
			exit(84);
		map[line_count] = my_strdup(buffer);
		line_count++;
	}
	map[line_count] = NULL;
	close(fd);
	free(buffer);
	return map;
}
