/*
** EPITECH PROJECT, 2018
** Matchstick
** File description:
** Game functions
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "matchstick.h"

int	check_win(int turn, char **map)
{
	int idx_x = 0;
	int idx_y = 0;

	while (map[idx_x]) {
		idx_y = 0;
		while (map[idx_x][idx_y]) {
			if (map[idx_x][idx_y] == '|') {
				return (0);
			}
			idx_y++;
		}
		idx_x++;
	}
	return (turn);
}

int	game_loop(char **map, int lines, int *max)
{
	static int turn = 1;

	display_map(map);
	write(1, "\n", 1);
	if (turn == 1) {
		my_printf("Your turn:\n");
		map = player_turn(map, lines, max);
		turn = 2;
	}
	else {
		map = bot_turn(map, lines, max);
		turn = 1;
	}
	if (map == NULL)
		turn = 0;
	return (turn);
}

int	game_function(int lines, int *max)
{
	char **map = create_map(lines);
	int win = 0;
	int turn = 1;

	while (win == 0) {
		turn = game_loop(map, lines, max);
		if (turn == 0)
			break;
		win = check_win(turn, map);
	}
	if (win != 0) {
		display_map(map);
	}
	free_map(map);
	return (win);
}
