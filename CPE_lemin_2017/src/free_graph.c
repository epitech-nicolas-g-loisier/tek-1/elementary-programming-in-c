/*
** EPITECH PROJECT, 2018
** lemin
** File description:
** Frees the graph_t array
*/

#include <stdlib.h>
#include "graph.h"

void	free_path(graph_t ***paths)
{
	for (int i = 0; paths[i] != NULL; i++)
		free(paths[i]);
	free(paths);
}

void	free_graph(graph_t **arr)
{
	int i = 0;

	while (arr[i] != NULL) {
		free(arr[i]->name);
		free(arr[i]->link);
		free(arr[i]);
		i++;
	}
	free(arr);
}
