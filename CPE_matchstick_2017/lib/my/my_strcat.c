/*
** EPITECH PROJECT, 2017
** lib
** File description:
** put a str at the end of an other str
*/

#include "my.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

char	*my_strcat(char const* first, char const* second)
{
	int	count = 0;
	int	counter = 0;
	int	length = my_strlen(first) + my_strlen(second);
	char	*dest = malloc(sizeof(char) * (length + 1));

	while (count < my_strlen(first)){
		dest[count] = first[count];
		count++;
	}
	while (counter < my_strlen(second)){
		dest[count] = second[counter];
		count++;
		counter++;
	}
	dest[count] = '\0';
	return (dest);
}
