/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** instruction 007
*/

#include <stdlib.h>
#include <unistd.h>
#include "corewar.h"
#include "my.h"
#include "instruction.h"
#include "label.h"

int	instruction_or(int fd, char *str, label_t *label, int *pos)
{
	int number_func = OR;
	char **param = NULL;
	int nb = 0;
	int byte = 2;

	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	nb = calc_description(param);
	write(fd, &number_func, 1);
	write(fd, &nb, 1);
	byte += write_parameter(param[1], fd, label, *pos);
	byte += write_parameter(param[2], fd, label, *pos);
	byte += write_parameter(param[3], fd, label, *pos);
	free_parameter(param);
	*pos += byte;
	return (0);
}
