/*
** EPITECH PROJECT, 2018
** lem_in
** File description:
** functions which put ant on path and move it
*/

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "lemin.h"
#include "graph.h"
#include "my.h"

void	add_ants(graph_t ***path, int *paths_size, int *ants_nbr)
{
	int path_idx = 0;
	static int ants = 0;

	if ((*ants_nbr) <= 0)
		return;
	while (path[path_idx] != NULL) {
		prune_active_path(paths_size, *ants_nbr);
		if (path[path_idx][0]->id == ID_END && *ants_nbr > 0) {
			ants ++;
			(*ants_nbr) --;
		}
		if (path[path_idx][0]->id == ID_NOR && *ants_nbr > 0) {
			path[path_idx][0]->ant_id = (ants + 1);
			path[path_idx][0]->id = ID_ISU;
			ants ++;
			(*ants_nbr) --;
		}
		path_idx ++;
	}
}

bool	move_ants(graph_t **path)
{
	bool ant_was_moved = false;
	int path_size = 0;

	while (path[path_size])
		path_size++;
	for (int i = path_size - 1; i > 0; i--) {
		if (path[i - 1]->id == ID_ISU) {
			path[i]->ant_id = path[i - 1]->ant_id;
			path[i - 1]->id = ID_NOR;
			if (path[i]->id == ID_NOR)
				path[i]->id = ID_ISU;
			ant_was_moved = true;
		}
	}
	return (ant_was_moved);
}
