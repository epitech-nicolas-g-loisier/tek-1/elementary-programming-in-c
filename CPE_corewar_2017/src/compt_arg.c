/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** compt argument given
*/

#include <stdlib.h>
#include "error_list.h"

int	count_param(char **param)
{
	int i = 1;

	while (param[i] != NULL) {
		i += 1;
	}
	return (i);
}

int	check_length(int nb_arg, char **param)
{
	int i = 0;

	i = count_param(param) - 1;
	if (i < nb_arg) {
		return (E_INARG);
	}
	else if (i > nb_arg) {
		return (E_TARG);
	}
	return (0);
}
