/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** check type of argument
*/

#include <unistd.h>
#include "my.h"
#include "corewar.h"
#include "label.h"

int	direct_to_indirect(char *str)
{
	if (str[0] == '%')
		return (1);
	else
		return (0);
}

int	get_label_diff(char *instruct, label_t *label, int pos)
{
	int delta = 0;

	while (label) {
		if (my_strcmp(&instruct[1], label->name) == 0)
			break;
		label = label->next;
	}
	if (!label)
		return (0);
	delta = label->pos - pos;
	return (delta);
}

int	check_if_label(char *instruct)
{
	if (instruct[0] == ':') {
		return (1);
	} else if (instruct[0] != '\0' && instruct[1] == ':') {
		return (1);
	} else {
		return (0);
	}
}

int	check_instruction(char *instruct)
{
	if (instruct[0] == '%')
		return (4);
	else if (instruct[0] == 'r')
		return (1);
	else
		return (2);
}

int	write_parameter(char *param, int fd, label_t *label, int pos)
{
	int nb = 0;
	int size = check_instruction(param);

	if (check_if_label(param) == 1)
		nb = get_label_diff(param, label, pos);
	else
		nb = my_getnbr(param);
	if (size == 1)
		write(fd, &nb, size);
	else
		write_big_endian(nb, size, fd);
	return (size);
}
