/*
** EPITECH PROJECT, 2018
** Matchstick
** File description:
** function for IA
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include "my.h"
#include "matchstick.h"

char	**bot_turn(char **map, int lines, int *max)
{
	int less = 1;
	int matches = 0;
	int line_nb = random() % lines + 1;

	srandom(time(NULL));
	my_printf("AI's turn...\n");
	while (max[line_nb] == 0) {
		line_nb = random() % lines + 1;
	}
	if (max[line_nb] == 1)
		less = 0;
	matches = random() % (max[line_nb] - less) % max[0] + 1;
	my_printf("AI removed %d match(es) from line %d\n", matches, line_nb);
	max[line_nb] -= matches;
	free(map[line_nb]);
	map[line_nb] = remove_matches(max[line_nb], lines, line_nb);
	return (map);
}
