/*
** EPITECH PROJECT, 2018
** Bootstrap Get Next Line
** File description:
** write a function that returns a stringa, in chunks of 5 characters.
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

char	*get_sentence_chunk_by_chunk(void)
{
	char	*sent = "Hello my name is Chucky. Do you want to be my friend?";
	char	*dest = malloc(sizeof(char) * 6);
	static int stop = 0;
	int	count = 0;

	while (count < 5 && sent[stop] != '\0'){
		dest[count] = sent[stop];
		count++;
		stop++;
	}
	return (dest);
}
