/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** instruction 005
*/

#include <stdlib.h>
#include <unistd.h>
#include "corewar.h"
#include "my.h"
#include "instruction.h"

int	instruction_sub(int fd, char *str, int *pos)
{
	int number_func = SUB;
	char **param = NULL;
	int nb = 0;

	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	nb = calc_description(param);
	write(fd, &number_func, 1);
	write(fd, &nb, 1);
	nb = my_getnbr(param[1]);
	write(fd, &nb, 1);
	nb = my_getnbr(param[2]);
	write(fd, &nb, 1);
	nb = my_getnbr(param[3]);
	write(fd, &nb, 1);
	free_parameter(param);
	*pos += 5;
	return (0);
}
