/*
** EPITECH PROJECT, 2018
** Bootstrap Get Next Line
** File description:
** returns the number of times it was called
*/

int	get_nb_calls(void)
{
	static int count = 0;
	count++;
	return (count);
}
