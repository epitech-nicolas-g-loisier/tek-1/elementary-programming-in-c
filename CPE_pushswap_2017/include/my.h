/*
** EPITECH PROJECT, 2017
** my
** File description:
** prototypes library
*/

void	my_putchar(char);
int	my_putnbr(int);
int	my_putstr(char*);
int	my_strlen(char const*);
int	my_getnbr(char const*);

