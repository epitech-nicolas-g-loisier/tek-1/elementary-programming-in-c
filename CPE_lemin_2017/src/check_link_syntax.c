/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** check the syntaxe of room name declaration
*/

#include <stdlib.h>
#include "graph.h"
#include "my.h"
#include "lemin.h"

int	check_end(char *str)
{
	int idx = 0;

	while (str[idx] != '\0') {
		if (str[idx] == '#')
			break;
		else if (str[idx] != ' ' && str[idx] != '\t')
			return (84);
		idx++;
	}
	return (0);
}

char	*get_room_name(char *str, int *idx)
{
	int count = 0;
	char *name = NULL;

	for (int tmp = *idx; str[tmp] != '\0' && str[tmp] != '-'; tmp++)
		count++;
	name = malloc(sizeof(char) * count + 1);
	if (name == NULL)
		return (NULL);
	count = 0;
	while (str[*idx] != '\0' && str[*idx] != '-' && str[*idx] != ' ' &&
		str[*idx] != '\t') {
		name[count] = str[*idx];
		*idx += 1;
		count++;
	}
	name[count] = '\0';
	return (name);
}

int	cmp_name(char *name[2], graph_t **room)
{
	int stop = 0;

	for (int idx = 0; room[idx] != NULL; idx++) {
		if (my_strcmp(room[idx]->name, name[0]) == 0) {
			stop = 1;
			break;
		}
	}
	if (stop != 1)
		return (84);
	for (int idx = 0; room[idx] != NULL; idx++) {
		if (my_strcmp(room[idx]->name, name[1]) == 0) {
			stop = 2;
			break;
		}
	}
	if (stop != 2)
		return (84);
	return (0);
}

int	check_link_syntax(char *str, graph_t **room)
{
	int idx = 0;
	char *name[2];
	int ret = 0;

	while (str[idx] == ' ' || str[idx] == '\t')
		idx++;
	name[0] = get_room_name(str, &idx);
	if (str[idx] != '-' || name[0] == NULL || room == NULL)
		return (84);
	idx++;
	name[1] = get_room_name(str, &idx);
	if (check_end(&str[idx]) != 0 || name[1] == NULL)
		return (84);
	else if (cmp_name(name, room) != 0)
		ret = 84;
	str[idx] = '\0';
	free(name[0]);
	free(name[1]);
	return (ret);
}
