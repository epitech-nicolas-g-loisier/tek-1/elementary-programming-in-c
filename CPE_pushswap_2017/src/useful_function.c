/*
** EPITECH PROJECT, 2017
** Push Swap
** File description:
** Useful function for Push Swap
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "pushswap.h"

list_t*	creat_root(void)
{
	list_t* root = malloc(sizeof(list_t));

	if (root == NULL)
		return (NULL);
	root->previous = root;
	root->next = root;
	return (root);
}

int	remove_to_list(list_t* list)
{
	list->previous->next = list->next;
	list->next->previous = list->previous;
	free(list);
	return (0);
}

int	add_to_list(list_t* list, int value)
{
	list_t* new_element = malloc(sizeof(list_t));

	if (new_element == NULL)
		return (84);
	new_element->value = value;
	new_element->previous = list;
	new_element->next = list->next;
	list->next->previous = new_element;
	list->next = new_element;
	return (0);
}

int	display_list(list_t* list)
{
	list_t* tmp_list = list->next;

	while (tmp_list != list){
		my_putnbr(tmp_list->value);
		my_putchar(' ');
		tmp_list = tmp_list->next;
	}
	my_putchar('\n');
	//free(tmp_list);
	return (0);
}
