/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** print the ant
*/

#include <stdlib.h>
#include <unistd.h>
#include "lemin.h"
#include "graph.h"
#include "my.h"

int	print_buffer(char *buffer, int *count)
{
	write(1, buffer, my_strlen(buffer));
	for (int i = 0; i < 4096; i++)
		buffer[i] = '\0';
	*count = 0;
	return (0);
}

int	add_int_to_char(int ant_nb, char *buffer, int *count)
{
	int tmp = ant_nb;
	int rank = 1;

	while ((ant_nb / rank) >= 10) {
		rank = rank * 10;
	}
	while (ant_nb > 9 && rank > 1) {
		tmp = ant_nb / rank % rank;
		buffer[*count] = tmp % 10 + '0';
		rank = rank / 10;
		*count += 1;
	}
	buffer[*count] = ant_nb % 10 + '0';
	*count += 1;
	buffer[*count] = '-';
	*count += 1;
	buffer[*count] = '\0';
	return (0);
}

int	put_ant(int ant_nb, char *room_name)
{
	static int count = 0;
	static char buffer[4096] = "\0";

	if (ant_nb == -1) {
		buffer[(count - 1)] = '\n';
	} else if (ant_nb == 0)
		print_buffer(&buffer[0], &count);
	else {
		if ((count + my_strlen(room_name)) > 4000)
			print_buffer(&buffer[0], &count);
		buffer[count] = 'P';
		count++;
		add_int_to_char(ant_nb, &buffer[0], &count);
		my_strcat(&buffer[count], room_name);
		count += my_strlen(room_name);
		buffer[count] = ' ';
		buffer[(count + 1)] = '\0';
		count += 1;
	}
	return (0);
}

int	first_ant_move(graph_t **path, int *print_ant)
{
	if (path == NULL ) {
		return (1);
	}
	put_ant(*print_ant, path[0]->name);
	*print_ant = *print_ant + 1;
	return (0);
}

int	print_ant(graph_t ***path, int *size, int nb_ant)
{
	static int print_ant = 1;
	int ret = 0;
	int pos = 0;

	if (path == NULL || size == NULL || nb_ant < 1) {
		return (84);
	}
	add_ant_to_buffer(path, size);
	while (print_ant <= nb_ant && ret == 0) {
		ret = first_ant_move(path[pos], &print_ant);
		pos++;
	}
	put_ant(-1, NULL);
	return (0);
}
