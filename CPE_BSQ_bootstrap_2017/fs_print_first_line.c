/*
** EPITECH PROJECT, 2017
** Bootstrap BSQ
** File description:
** print first line of a file
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void	fs_print_first_line(char const* filepath)
{
	int	fd = 0;
	int	size = 1;
	char	*buffer = malloc(sizeof(char) * 2);

	fd = open(filepath, O_RDONLY);
	if (fd == -1)
		exit(84);
	while (buffer[0] != '\n' && size != 0){
		size = read(fd, buffer, 1);
		if (size == -1)
			exit(84);
		write (1, buffer, 1);
	}
}
