/*
** EPITECH PROJECT, 2017
** Bootstrap BSQ
** File description:
** check if can open dir
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int	fs_open_file(char const *filepath)
{
	int	check = 0;

	check = open(filepath, O_RDONLY);
	if (check == -1){
		write(1, "FAILURE\n", 8);
	} else
		write(1, "SUCCESS\n", 8);
	close(filepath);
	return check;
}
