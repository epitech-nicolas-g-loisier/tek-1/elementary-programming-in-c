/*
** EPITECH PROJECT, 2017
** header 
** File description:
** header for bsq
*/

#ifndef BSQ_H_
#define BSQ_H_

/* square structure */
typedef struct square
{
	int	size;
	int	x;
	int	y;
}square_t;

/* bsq.c */
void    find_square(int, int, char**, square_t*);
void    modify_map(char**, square_t);
void    display_result(char**);
void    find_largest_square(char const*);

/* get_data_from_file.c */
int	get_nbr_from_first_line(char const*);
int	get_length_of_first_line(char const*);
char    **take_map(int, int, char const*);

/* count_digit.c */
int	count_digit(int);

/* memory_tab.c */
void    free_tab(char**);
void	free_int_tab(int**);

#endif /* BSQ_H_ */
