/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** instruction 004
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"

int	instruction_add(int fd, char *str, int *pos)
{
	int number_func = ADD;
	int result = 0;
	char **param = NULL;
	int nb = 0;

	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	nb = calc_description(param);
	write(fd, &number_func, 1);
	write(fd, &nb, 1);
	result = my_getnbr(param[1]);
	write(fd, &result, 1);
	result = my_getnbr(param[2]);
	write(fd, &result, 1);
	result = my_getnbr(param[3]);
	write(fd, &result, 1);
	free_parameter(param);
	*pos += 5;
	return (0);
}
