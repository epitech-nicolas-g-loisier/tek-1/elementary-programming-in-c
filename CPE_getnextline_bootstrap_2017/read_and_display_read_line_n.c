/*
** EPITECH PROJECT, 2018
** Bootstrap Get Next Line
** File description:
** read and write one line
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

void	read_and_display_read_line_n(int fd, int n)
{
	char	*buffer = malloc(sizeof(char) * n + 1);
	int	size = n;
	int	count = 0;

	while (buffer[count] != '\n' && size == n){
		count = 0;
		size = read(fd, buffer, n);
		buffer[n] = '\0';
		while (buffer[count] != '\0' && buffer[count] != '\n'){
			write(1, &buffer[count], 1);
			count++;
		}
	}
	free(buffer);
}
