/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Checks if the arg given is a label
*/

#include <stdbool.h>
#include "corewar.h"

bool	str_is_label(char *str)
{
	if (!str || str[0] != ':')
		return (false);
	if (str_is_alphanum(&str[1]))
		return (true);
	return (false);
}
