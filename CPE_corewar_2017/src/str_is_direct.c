/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Checks if the arg given is a direct
*/

#include <stdbool.h>
#include "my.h"
#include "corewar.h"

bool	str_is_direct(char *param, int *progress)
{
	if (!param || param[0] != '%' || my_strlen(param) < 2)
		return (false);
	if (my_str_isnum(&param[1]) || str_is_label(&param[1])) {
		*progress = *progress + 4;
		return (true);
	}
	return (false);
}
