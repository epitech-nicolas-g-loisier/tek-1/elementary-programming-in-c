/*
** EPITECH PROJECT, 2018
** corewar assembler
** File description:
** function which display error message
*/

#include "check_file.h"
#include "error_list.h"
#include "my.h"

void	error_message(char *pathname, int nb_line, char *err_type)
{
	write(2, "asm, ", 5);
	write(2, pathname, my_strlen(pathname));
	write(2, ", line ", 7);
	my_put_nbr(nb_line);
	write(2, ": ", 2);
	write(2, err_type, my_strlen(err_type));
	write(2, "\n", 1);
}

char	*message_tab(int error)
{
	char *msg[E_MAX_ID] = {"The comment must be just after the name.",
			"Invalid instruction.", "Invalid label name.",
			"Multiple definition of the same label.",
			"The name can only be defined once",
			"The comment can only be defined once.",
			"The program name is too long.",
			"The comment is too long.",
			"The argument given to the instruction is invalid.",
			"Undefined label.", "Invalid register number.",
			"Too many arguments given to the instruction.",
			"The file is empty.", "No comment specified.",
			"Syntax error.", "Out of memory."};
	return (msg[error]);
}

int	error_fptr(char *pathname, int nb_line, int error)
{
	error--;
	if (error == -1)
		return (0);
	if (error > -1 && error < E_MAX_ID)
		error_message(pathname, nb_line, message_tab(error));
	return (84);
}
