/*
** EPITECH PROJECT, 2018
** Matchstick
** File description:
** function for player
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "matchstick.h"
#include "get_next_line.h"

int	get_line_nb(int lines)
{
	char *line = NULL;
	int entry = 0;

	my_printf("Line: ");
	line = get_next_line(0);
	if (line == NULL)
		return (-2);
	if (my_str_isposnum(line) == 0) {
		my_printf("Error: invalid input (positive number expected)\n");
		free(line);
		return (-1);
	} else {
		entry = take_number(line);
		free(line);
		if (entry < 1 || entry > lines) {
			my_printf("Error: this line is out of range\n");
			return (-1);
		}
	}
	return (entry);
}

int	check_entry(int entry, int *max, int line)
{
	if (entry < 1 || entry > max[0]) {
		my_printf("Error: you cannot remove more than %d " \
				"matches per turn\n", max[0]);
		return (-1);
	} else if (entry > max[line]) {
		my_printf("Error: not enough matches on this line\n"
				, max[line], line);
		return (-1);
	}
	return entry;
}

int	get_matches_nb(int *max, int line)
{
	char *matches = NULL;
	int entry = 0;

	write(1, "Matches: ", 9);
	matches = get_next_line(0);
	if (matches == NULL)
		return (-2);
	if (my_str_isposnum(matches) == 0) {
		my_printf("Error: invalid input (positive number expected)\n");
		free(matches);
		return (-1);
	} else {
		entry = take_number(matches);
		free(matches);
		entry = check_entry(entry, max, line);
	}
	return (entry);
}

char	**player_turn(char **map, int lines, int *max)
{
	int end = 0;
	int line_nb = 0;

	while (end <= 0) {
		line_nb = get_line_nb(lines);
		if (line_nb == -1)
			end = -1;
		else if (line_nb == -2) {
			return (NULL);
		} else {
			end = get_matches_nb(max, line_nb);
			if (end == -2)
				return (NULL);
		}
	}
	my_printf("Player removed %d match(es) from line %d\n", end, line_nb);
	max[line_nb] -= end;
	free(map[line_nb]);
	map[line_nb] = remove_matches(max[line_nb], lines, line_nb);
	return (map);
}
