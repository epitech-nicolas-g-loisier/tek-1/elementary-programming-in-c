/*
** EPITECH PROJECT, 2018
** Matchstick
** File description:
** Header for matchstick
*/

#ifndef MATCHSTICK_H_
#define MATCHSTICK_H_

/* bot_function.c */
char    **bot_turn(char**, int, int*);

/* game_function.c */
int     check_win(int, char**);
int     game_function(int, int*);

/* main.c */
int     take_number(char*);
int     *get_max_matches(int, int);
int     matchstick(char*, char*);

/* map_managing.c */
char    *full_line(int, int);
char    *extrem_line(int);
char    **create_map(int);
void    display_map(char**);
void    free_map(char**);

/* player_function.c */
int     get_line_nb(int);
int     get_matches_nb(int*, int);
char    **player_turn(char**, int, int*);

/* remove_matches.c */
char    *remove_matches(int, int, int);

#endif /* MATCHSTICK_H_ */
