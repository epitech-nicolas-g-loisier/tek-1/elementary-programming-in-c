/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Converts hex to dec
*/

int	conv_hex_to_dec(unsigned char *hex)
{
	int nbr = 0;
	int base = 1;
	int count = 3;

	while (count >= 0) {
		if (hex[count] >= '0' && hex[count] <= '9')
			nbr = nbr + (hex[count] - '0') * base;
		else
			nbr = nbr + (hex[count] - 'a' + 10) * base;
		count--;
		base = base * 16;
	}
	return (nbr);
}
