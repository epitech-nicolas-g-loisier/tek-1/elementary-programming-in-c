/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** check if live have good argument
*/

#include <unistd.h>
#include "corewar.h"
#include "error_list.h"

int	check_live(char *str, int *pos)
{
	char **param = parameter_parser(str);
	int ret = 0;

	*pos = *pos + 1;
	if (param == NULL)
		return (E_MEM);
	else if (check_length(1, param) != 0) {
		ret = check_length(1, param);
		free_parameter(param);
		return (ret);
	}
	ret = str_is_param(param[1], pos);
	free_parameter(param);
	return (ret);
}
