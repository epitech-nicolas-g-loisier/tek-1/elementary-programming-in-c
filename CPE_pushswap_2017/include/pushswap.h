/*
** EPITECH PROJECT, 2017
** Push Swap
** File description:
** Header for Push Swap
*/

#ifndef PUSH_SWAP_
#define PUSH_SWAP_

/* Structure for double circular chain list */
typedef struct list_t
{
	int value;
	struct list_t* previous;
	struct list_t* next;
} list_t;

/* Useful functions */

list_t* creat_root(void);
int	remove_to_list(list_t*);
int	add_to_list(list_t*, int);
int	display_list(list_t*);

/* Swap functions */
void	swap_list(list_t*);
void	swap_two_list(list_t*, list_t*);
void	swap_list_a(list_t*);
void	swap_list_b(list_t*);

/* Switch functions */
void	switch_listb_to_lista(list_t*, list_t*);
void	switch_lista_to_listb(list_t*, list_t*);

/* Rotate functions */
void	rotate_list(list_t*);
void	rotate_two_list(list_t*, list_t*);
void	rotate_list_a(list_t*);
void	rotate_list_b(list_t*);

/* Reverse rotate functions */
void	reverse_rotate_list(list_t*);
void	reverse_rotate_two_list(list_t*, list_t*);
void	reverse_rotate_list_a(list_t*);
void	reverse_rotate_list_b(list_t*);

#endif /* PUSH_SWAP */
