/*
** EPITECH PROJECT, 2018
** corewar assembler
** File description:
** error_gestion of corewar asm
*/

#include "check_file.h"
#include "get_next_line.h"
#include "error_list.h"
#include "label.h"
#include "my.h"

static int	reset_name_status(void)
{
	invalid_header(NULL, E_MNAME);
	return (84);
}

int	invalid_instruction(char *line, int error)
{
	int idx = 0;

	if (error > 0 && error <= E_MAX_ID)
		return (error);
	while (idx < my_strlen(line)) {
		if (line[idx] == ':' && line[idx - 1] == ' ')
			return (E_INST);
		idx ++;
	}
	return (0);
}

int	error_checker(char *line, label_t *label, int *pos, int error)
{
	bool lab = false;

	error = invalid_header_instruction(line);
	error = invalid_header(line, error);
	if ((error > 0 && error <= E_MAX_ID) || line[0]=='#' || line[0] == '\0')
		return (error);
	error = check_label_instruction(line, &lab);
	if (lab) {
		error = check_label(line, label, *pos, error);
	}
	error = invalid_instruction(line, error);
	error = check_arguments(line, &pos[0], error);
	return (error);
}

int	check_file_content(char *pathname, label_t *label, int *pos)
{
	char *line = NULL;
	int fd = open(pathname, O_RDONLY);
	int error = 0;
	int nb_line = 0;

	if (fd == -1)
		return (84);
	line = get_next_line(fd);
	while (line != NULL) {
		nb_line ++;
		error = error_checker(line, label, &pos[0], error);
		error = error_fptr(pathname, nb_line, error);
		if (error == 84)
			return (reset_name_status());
		free(line);
		line = get_next_line(fd);
	}
	close(fd);
	invalid_header(NULL, E_MNAME);
	return (0);
}
