/*
** EPITECH PROJECT, 2018
** Put Float
** File description:
** Put float with 2 decimal
*/

#include <unistd.h>
#include "my.h"
#include "n4s.h"

void	put_float(double nb)
{
	if (nb < 0)
		write(1, "-", 1);
	nb = ABS(nb);
	if (nb >= 1.0) {
		write(1, "1.00", 4);
	} else {
		write(1, "0.", 2);
		if (nb < 0.1)
			write(1, "0", 1);
		if (nb < 0.01)
			write(1, "0", 1);
		my_put_nbr((int)(nb * 1000));
	}
	write(1, "\n", 1);
}
