/*
** EPITECH PROJECT, 2017
** Library
** File description:
** my_getnbr
*/

int	check_nbr(char c)
{
	int	i;

	if (c > 47 && c < 58){
		i = c - 48;
		return (i);
	}
	else if (c == 45)
		return (-1);
	return (84);
}

int	my_getnbr(char const *str)
{
	int	i = 0;
	int	nbr = 0;
	int	test = 0;
	int	neg = 1;

	while (str[i] != 0){
		test = check_nbr(str[i]);
		if (test == -1)
			neg = -1;
		else {
			nbr = nbr * 10;
			nbr += test;
		}
		i++;
	}
	return ((nbr * neg));
}
