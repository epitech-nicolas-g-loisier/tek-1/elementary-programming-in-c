/*
** EPITECH PROJECT, 2018
** Corewar Assemebler
** File description:
** check if aff have good argument
*/

#include <stdlib.h>
#include "corewar.h"
#include "error_list.h"

int	check_aff(char *str, int *pos)
{
	char **param = parameter_parser(str);
	int ret = 0;

	*pos = *pos + 2;
	if (param == NULL)
		return (E_MEM);
	else if (check_length(1, param) != 0) {
		ret = check_length(1, param);
		free_parameter(param);
		return (ret);
	}
	ret = str_is_register(param[1], pos);
	free_parameter(param);
	return (ret);
}
