/*
** EPITECH PROJECT, 2018
** need4stek
** File description:
** function which do an average of radar values
*/

#include <stdlib.h>
#include <stdio.h>
#include "n4s.h"
#include "my.h"

void	radar_average(char *line, int *parsing, double radar[2])
{
	double total = 0;
	double count = 0;

	for (int idx = 3; idx <= 18; idx ++) {
		if (my_strcmp("3010.0", &line[parsing[idx]]) != 0) {
			total += get_float(&line[parsing[idx]]);
			count++;
		}
	}
	radar[0] = total / (count > 1 ? count : 0);
	total = 0;
	count = 0;
	for (int idx = 19; idx <= 34; idx ++) {
		if (my_strcmp("3010.0", &line[parsing[idx]]) != 0) {
			total += get_float(&line[parsing[idx]]);
			count++;
		}
	}
	radar[1] = total / (count > 1 ? count : 0);
}
