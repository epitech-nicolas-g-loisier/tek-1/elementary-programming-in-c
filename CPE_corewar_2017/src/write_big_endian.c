/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** Write data in big endian
*/

#include "my.h"
#include <unistd.h>

static char	find_hexa_low(long power, int nb)
{
	long tmp = 0;
	long div = my_compute_power_rec(16, power);
	char res;
	char *chara = "0123456789abcdef";

	tmp = nb / div;
	res = chara[tmp];
	return (res);
}

static int	conv_int_to_hexa(char hexa[9], int data)
{
	int power = 0;
	int tmp_nb = data;

	while (data != 0){
		power = 0;
		tmp_nb = data;
		while (tmp_nb >= 16){
			tmp_nb = tmp_nb / 16;
			power++;
		} if (power == 0){
			hexa[0] = find_hexa_low(0, data);
			data = 0;
		} else {
			hexa[power] = find_hexa_low(power, data);
			data = data % my_compute_power_rec(16, power);
		}
	}
	hexa = my_revstr(hexa);
	return (0);
}

static int	find_dec(char hexa)
{
	char *chara = "0123456789abcdef";
	int idx = 0;

	while (chara[idx] != '\0') {
		if (chara[idx] == hexa)
			return (idx);
		idx++;
	}
	return (0);
}

static int	conv_hexa_to_big_end(char hexa[9], int big_end[4])
{
	big_end[0] = find_dec(hexa[0]) * 16;
	big_end[0] += find_dec(hexa[1]);
	big_end[1] = find_dec(hexa[2]) * 16;
	big_end[1] += find_dec(hexa[3]);
	big_end[2] = find_dec(hexa[4]) * 16;
	big_end[2] += find_dec(hexa[5]);
	big_end[3] = find_dec(hexa[6]) * 16;
	big_end[3] += find_dec(hexa[7]);
	return (0);
}

int	write_big_endian(int data, int size, int fd)
{
	char hexa[9] = "00000000\0";
	int big_end[4];

	if (data < 0)
		data = 2147483648 + data;
	conv_int_to_hexa(&hexa[0], data);
	conv_hexa_to_big_end(hexa, &big_end[0]);
	if (size == 4) {
		write(fd, &big_end[0], 1);
		write(fd, &big_end[1], 1);
		write(fd, &big_end[2], 1);
		write(fd, &big_end[3], 1);
	}
	else if (size == 2) {
			write(fd, &big_end[2], 1);
			write(fd, &big_end[3], 1);
	}
	return (0);
}
