/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Checks if the arg given is a parameter
*/

#include "corewar.h"

int	str_is_param(char *param, int *progress)
{
	if (str_is_indirect(param, progress))
		return (0);
	if (str_is_direct(param, progress))
		return (0);
	return (str_is_register(param, progress));
}
