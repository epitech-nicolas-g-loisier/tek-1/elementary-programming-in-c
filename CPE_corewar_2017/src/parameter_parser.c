/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** Parser for parameter of instructions
*/

#include <stdlib.h>
#include <fcntl.h>

void	free_parameter(char **param)
{
	int idx = 0;

	while (param[idx] != NULL) {
		free(param[idx]);
		idx++;
	}
	free(param);
}

int	check_para_separator(char para)
{
	char list[5] = ", \t\0";
	int idx = 0;

	while (idx < 4) {
		if (list[idx] == para)
			return (1);
		idx++;
	}
	return (0);
}

static char	*get_parameter(char *str, int *idx)
{
	char *para;
	int count = 0;

	while (check_para_separator(str[count]) == 0) {
		count++;
	}
	para = malloc(sizeof(char) * (count + 1));
	if (para == NULL)
		return (NULL);
	count = 0;
	while (check_para_separator(str[count]) == 0) {
		para[count] = str[count];
		count++;
	}
	para[count] = '\0';
	while (check_para_separator(str[count]) == 1 && str[count] != '\0') {
		count++;
	}
	*idx += count - 1;
	return (para);
}

static char	*check_para_label(char *para, char *str, int *idx)
{
	int count = 0;

	while (para[count] != '\0') {
		count++;
	}
	if (para[(count - 1)] != ':') {
		return (para);
	} else {
		free(para);
		count = 0;
		while (check_para_separator(str[count]) == 1) {
			count++;
		}
		para = get_parameter(&str[count], &count);
	}
	*idx += count;
	return (para);
}

char	**parameter_parser(char *str)
{
	int idx = 0;
	int count = 1;
	char **para = malloc(sizeof(char*) * 6);

	if (para == NULL)
		return (NULL);
	while (check_para_separator(str[idx]) == 1) {
		idx++;
	}
	para[0] = get_parameter(&str[idx], &idx);
	para[0] = check_para_label(para[0], &str[idx], &idx);
	while (str[(idx - 1)] != '\0' && count < 6) {
		if (str[idx] != ' ' && str[idx] != '\t' && str[idx] != '\0') {
			para[count] = get_parameter(&str[idx], &idx);
			count++;
		}
		idx++;
	}
	para[count] = NULL;
	return (para);
}
