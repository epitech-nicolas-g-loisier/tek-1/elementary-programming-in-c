/*
** EPITECH PROJECT, 2018
** lemin
** File description:
** Declares the graph structure
*/

#ifndef GRAPH_ID_
#define GRAPH_ID_

enum graph_id
{
	ID_NOR = 0,
	ID_ISR = 1,
	ID_ISU = 2,
	ID_STA = 3,
	ID_END = 4
};

#endif /* GRAPH_ID_ */

#ifndef L_GRAPH_
#define L_GRAPH_

typedef struct graph
{
	char *name;
	int id;
	int ant_id;
	struct graph **link;
} graph_t;

#endif /* L_GRAPH_ */
