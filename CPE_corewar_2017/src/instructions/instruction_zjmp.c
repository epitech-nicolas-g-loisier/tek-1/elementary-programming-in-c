/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** function for the zjmp instruction
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"
#include "label.h"

int	instruction_zjmp(int fd, char *str, label_t *label, int *pos)
{
	int instruct = ZJMP;
	char **param;
	int byte = 1;
	int tmp = 0;

	write(fd, &instruct, 1);
	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	tmp = direct_to_indirect(param[1]);
	byte += write_parameter(&param[1][tmp], fd, label, *pos);
	free_parameter(param);
	*pos += byte;
	return (0);
}
