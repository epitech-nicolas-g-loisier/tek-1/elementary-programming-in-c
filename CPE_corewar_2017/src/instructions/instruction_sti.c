/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** function for sti instruction
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"
#include "label.h"

int	instruction_sti(int fd, char *str, label_t *label, int *pos)
{
	int instruct = STI;
	char **param;
	int nb = 0;
	int byte = 2;
	int tmp = 0;

	write(fd, &instruct, 1);
	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	nb = calc_description(param);
	write(fd, &nb, 1);
	byte += write_parameter(param[1], fd, label, *pos);
	tmp = direct_to_indirect(param[2]);
	byte += write_parameter(&param[2][tmp], fd, label, *pos);
	tmp = direct_to_indirect(param[3]);
	byte += write_parameter(&param[3][tmp], fd, label, *pos);
	free_parameter(param);
	*pos += byte;
	return (0);
}
