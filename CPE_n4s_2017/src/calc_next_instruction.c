/*
** EPITECH PROJECT, 2018
** need4stek
** File description:
** main function of the project
*/

#include "n4s.h"
#include "calc_instruct.h"

double	pow(double number, double power)
{
	double total = 1;

	while (power > 0) {
		total = total * number;
		power--;
	}
	return (total);
}

static double	get_rotation(double radar[2], double speed, double old_speed)
{
	double rotation = 0;
	double calc_dist = radar[1] / BRAKE_DIST;

	rotation = (calc_dist > MAX_RAW_ROT ? 0 : MAX_RAW_ROT - calc_dist);
	calc_dist = radar[0] / BRAKE_DIST;
	rotation -= (calc_dist > MAX_RAW_ROT ? 0 : MAX_RAW_ROT - calc_dist);
	if (speed > old_speed + 0.05)
		return (rotation);
	if (speed < BRAKE_SPEED) {
		rotation += (ROTATE_CALC(speed)) * (rotation > 0 ? 1 : -1);
	}
	return (rotation);
}

double	calc_next_instruction(char *line, int *parsing, double *rotation)
{
	static double old_speed = 1;
	double speed = 0;
	double radar[2] = {0, 0};

	speed = get_float(&line[parsing[17]]) + get_float(&line[parsing[18]]);
	speed += get_float(&line[parsing[19]]) + get_float(&line[parsing[20]]);
	speed = speed / SPEED_RATE;
	radar_average(line, parsing, &radar[0]);
	*rotation = get_rotation(radar, speed, old_speed);
	if (speed > old_speed + MAX_ACC) {
		speed = old_speed + MAX_ACC;
	}
	old_speed = speed;
	return (speed);
}
