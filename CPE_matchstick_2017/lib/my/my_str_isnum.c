/*
** EPITECH PROJECT, 2017
** Libraries
** File description:
** str_isnum
*/

int	my_str_isposnum(char const *str)
{
	int	i = 0;

	while (str[i] != '\0'){
		if (str[i] > 47 && str[i] < 58)
			i++;
		else
			return (0);
	}
	return (1);
}

int	my_str_isnum(char const *str)
{
	int	i = 0;

	if (str[0] == '-')
		i++;
	while (str[i] != '\0'){
		if (str[i] > 47 && str[i] < 58)
			i++;
		else
			return (0);
	}
	return (1);
}
