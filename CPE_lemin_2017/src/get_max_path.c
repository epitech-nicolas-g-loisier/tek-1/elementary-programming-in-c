/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** get the max of possible path
*/

#include <stdlib.h>
#include "graph.h"

static int get_nb_path(graph_t *room)
{
	int idx = 0;

	if (room == NULL || room->link == NULL)
		return (0);
	while (room->link[idx] != NULL)
		idx++;
	return (idx);
}

int	get_max_path(graph_t **rooms)
{
	int end = 0;
	int max_end = 0;
	int max_start = 0;

	if (rooms == NULL)
		return (0);
	while (rooms[end] != NULL && rooms[end]->id != ID_END)
		end++;
	max_end = get_nb_path(rooms[end]);
	max_start = get_nb_path(rooms[0]);
	if (max_end < max_start)
		return (max_end);
	else
		return (max_start);
}
