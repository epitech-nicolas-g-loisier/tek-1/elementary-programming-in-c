/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_put_nbr.c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	my_putnbr_p(va_list ap)
{
	int	nb = va_arg(ap, int);
	int	c;
	int	div = 1;
	int	nbr;

	if (nb < 0){
		nb = -nb;
		my_putchar('-');
	}
	nbr = nb;
	while (nb >= 10){
		nb = nb / 10;
		div = div * 10;
	}
	while (div != 0){
		c = nbr / div;
		nbr = nbr - c * div;
		div = div / 10;
		my_putchar(c + 48);
	}
	return (0);
}
