/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** Calcul the value of the instruction's description
*/

#include "my.h"
#include <stdlib.h>

int	conv_byte(char *byte)
{
	int nb = 0;

	nb += (byte[7] - '0');
	nb += (byte[6] - '0') * 2;
	nb += (byte[5] - '0') * 4;
	nb += (byte[4] - '0') * 8;
	nb += (byte[3] - '0') * 16;
	nb += (byte[2] - '0') * 32;
	nb += (byte[1] - '0') * 64;
	nb += (byte[0] - '0') * 128;
	return (nb);
}

void	add_descr_code(char *desc, char para)
{
	char code[3][3] = {{"01\0"}, {"10\0"}, {"11\0"}};

	if (para == 'r')
		my_strcat(&desc[0], code[0]);
	else if (para == '%')
		my_strcat(&desc[0], code[1]);
	else
		my_strcat(&desc[0], code[2]);
}

int	calc_description(char **parameter)
{
	char code[3] = "00\0";
	char desc[8] = "\0";
	int idx = 1;
	int count = 0;
	int ret = 0;

	while (parameter[idx] != NULL) {
		add_descr_code(&desc[0], parameter[idx][0]);
		count += 2;
		idx++;
	}
	while (count < 8) {
		my_strcat(&desc[0], code);
		count += 2;
	}
	ret = conv_byte(desc);
	return (ret);
}
