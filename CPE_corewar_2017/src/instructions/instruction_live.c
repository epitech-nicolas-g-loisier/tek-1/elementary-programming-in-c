/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** instruction 001
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"
#include "label.h"

int	instruction_live(int fd, char *str, label_t *label, int *pos)
{
	int number_func = LIVE;
	char **param = NULL;
	int byte = 1;

	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	write(fd, &number_func, 1);
	byte += write_parameter(param[1], fd, label, *pos);
	free_parameter(param);
	*pos += byte;
	return (0);
}
