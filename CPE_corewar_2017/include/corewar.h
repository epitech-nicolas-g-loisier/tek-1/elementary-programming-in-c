/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Prototypes the functions used in the project
*/

#include <stdbool.h>
#include "label.h"

#ifndef COREWAR_
#define COREWAR_

int	ignore_indent(char *);

int	check_file_content(char *, label_t *, int *);

int	create_binary(char *);

int	init_binary(int, int, char **, int);

int	instruction_add(int, char *, int*);

int	instruction_aff(int, char *, int*);

int	instruction_and(int, char *, label_t*, int*);

int	instruction_fork(int, char *, label_t*, int*);

int	instruction_ld(int, char *, label_t*, int*);

int	instruction_ldi(int, char *, label_t*, int*);

int	instruction_lfork(int, char *, label_t*, int*);

int	instruction_live(int, char *, label_t*, int*);

int	instruction_lld(int, char *, label_t*, int*);

int	instruction_lldi(int, char *, label_t*, int*);

int	instruction_or(int, char *, label_t*, int*);

int	instruction_st(int, char *, label_t*, int*);

int	instruction_sti(int, char *, label_t*, int*);

int	instruction_sub(int, char *, int*);

int	instruction_xor(int, char *, label_t*, int*);

int	instruction_zjmp(int, char *, label_t*, int*);

int	direct_to_indirect(char*);

int     check_instruction(char*);

int	calc_description(char**);

char	**parameter_parser(char*);

void	free_parameter(char**);

int	write_parameter(char*, int, label_t*, int);

void	write_big_endian(int, int, int);

int	(*get_long_function_pointer(int))(int, char *, label_t *, int *);

int	(*get_short_function_pointer(int))(int, char *, int *);

bool	is_short_function_index(int *);

bool    str_is_alphanum(char *);

int	str_is_param(char*, int*);

bool	str_is_register(char*, int*);

bool	str_is_indirect(char*, int*);

bool	str_is_index(char*, int*);

bool	str_is_label(char*);

bool	str_is_direct(char*, int*);

int	count_param(char**, int);

char	*get_instruct_name(int);

int	check_para_separator(char);

int	check_length(int, char**);

int	check_nb_function(char*);

bool	check_used_label(char *, label_t *);

int	build_champion(char *, label_t *, int);

#endif //COREWAR_
