/*
** EPITECH PROJECT, 2018
** corewar assembler
** File description:
** file which contain error value enumeration
*/

#ifndef __ERROR_LIST__
#define __ERROR_LIST__

enum error_val {
	E_NAME = 1,
	E_INST = 2,
	E_LNAME = 3,
	E_MLABL = 4,
	E_MNAME = 5,
	E_MCMNT = 6,
	E_NLEN = 7,
	E_CLEN = 8,
	E_INARG = 9,
	E_ULABL = 10,
	E_INREG = 11,
	E_TARG = 12,
	E_EMPTY = 13,
	E_CMNT = 14,
	E_SYNT = 15,
	E_MEM = 16,
	E_MAX_ID = 16
};

#endif
