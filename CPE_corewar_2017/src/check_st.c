/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** check if st have good argument
*/

#include <stdlib.h>
#include "corewar.h"
#include "error_list.h"

int	check_st(char *str, int *pos)
{
	char **param = parameter_parser(str);
	int ret = 0;

	*pos = *pos + 2;
	if (param == NULL)
		return (E_MEM);
	else if (check_length(2, param) != 0) {
		ret = check_length(2, param);
		free_parameter(param);
		return (ret);
	}
	ret = str_is_register(param[1], pos);
	if (ret != 0) {
		free_parameter(param);
		return (ret);
	}
	ret = str_is_param(param[2], pos);
	free_parameter(param);
	return (ret);
}
