/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** get file data
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "get_next_line.h"
#include "graph.h"
#include "my.h"
#include "lemin.h"

int	get_nb_ant(char *buffer, int *line)
{
	int nb = 0;

	if (buffer == NULL)
		return (0);
	else if (my_str_isnum(buffer) == 0) {
		write (2, "error: Invalid line for number of ant\n", 38);
		return (0);
	}
	write(1, "#number_of_ants\n", 16);
	write(1, buffer, my_strlen(buffer));
	write(1, "\n", 1);
	write(1, "#rooms\n", 7);
	nb = my_getnbr(buffer);
	*line += 1;
	return (nb);
}

graph_t	**find_start(graph_t **rooms)
{
	int idx = 0;
	graph_t *start = NULL;

	if (rooms == NULL)
		return (NULL);
	while (rooms[idx] != NULL) {
		if (rooms[idx]->id == ID_STA) {
			start = rooms[idx];
			break;
		}
		idx++;
	}
	if (rooms[idx] == NULL)
		return (rooms);
	rooms[idx] = rooms[0];
	rooms[0] = start;
	return (rooms);
}

int	get_data_line(graph_t ***rooms, int *id, char *buffer, int *nb_ant)
{
	static int line = 0;
	int ret = 0;

	if (*id == -1) {
		write(2, "error: Multiple définition of start or end\n", 43);
		return (84);
	} else if (line == 0) {
		*nb_ant = get_nb_ant(buffer, &line);
	} else {
		if (check_room_syntax(&buffer[0]) == 0 && line == 1) {
			*rooms = get_room_pointer(buffer, *id, *rooms);
		} else if (check_link_syntax(buffer, *rooms) == 0) {
			ret = get_room_link(buffer, *rooms, &line);
		} else {
			write(2, "error: Invalid line given\n", 26);
			return (84);
		}
		*id = 0;
	}
	return (ret);
}

graph_t	**get_file_data(int *nb_ant)
{
	graph_t **rooms = NULL;
	char *buffer = NULL;
	int id = 0;
	int ret = 0;

	buffer = get_next_line(0);
	while (buffer != NULL) {
		if (buffer[0] == '#' && buffer[1] == '#')
			id = get_id(buffer);
		else if (buffer[0] != '#') {
			ret = get_data_line(&rooms, &id, buffer, nb_ant);
			if (*nb_ant < 1 || ret == 84)
				return (NULL);
		}
		free(buffer);
		buffer = get_next_line(0);
	}
	return (find_start(rooms));
}
