/*
** EPITECH PROJECT, 2018
** lemin
** File description:
** Finds the shortest path to get to the end
*/

#include <stdlib.h>
#include <stdbool.h>
#include "lemin.h"
#include "graph.h"

static bool	is_path_smaller(int size, int smallest)
{
	if (smallest == 0)
		return (true);
	if (size < smallest - 2)
		return (true);
	return (false);
}

static int	update_shortest_path(graph_t ***temp, graph_t ***path)
{
	if (!*temp)
		return (0);
	if (*path)
		free(*path);
	*path = *temp;
	*temp = NULL;
	return (0);
}

static graph_t	**find_shortest_path(graph_t *room, int size, int *smallest,
			graph_t **graph)
{
	graph_t **temp = NULL;
	graph_t **path = NULL;

	if (room->id == ID_NOR)
		room->id = ID_ISR;
	for (int i = 0; room->link[i] != NULL; i++) {
		if (is_path_smaller(size, *smallest) && room->link[i]->id == 0)
			temp = find_shortest_path(room->link[i], size + 1,
						smallest, graph);
		update_shortest_path(&temp, &path);
		if (room->link[i]->id == ID_END) {
			path = get_path(graph, size);
			*smallest = size + 1;
			break;
		}
	}
	if (room->id == ID_ISR)
		room->id = ID_NOR;
	return (path);
}

static void	prune_graph(graph_t **path, graph_t *start)
{
	for (int i = 0; path[i]->id == ID_NOR; i++)
		path[i]->id = ID_ISU;
	for (int i = 0; start->link[i] != NULL; i++) {
		if (start->link[i]->id == ID_END) {
			while (start->link[i] != NULL) {
				start->link[i] = start->link[i + 1];
				i++;
			}
			i--;
		}
	}
}

graph_t	***find_max_path(graph_t **graph)
{
	int counter = 0;
	int max_path = get_max_path(graph);
	int size = 0;
	graph_t ***paths = NULL;

	paths = malloc(sizeof(graph_t *) * (max_path + 1));
	if (!paths)
		return (NULL);
	paths[max_path] = NULL;
	while (counter < max_path) {
		paths[counter] = find_shortest_path(*graph, 0, &size, graph);
		if (!paths[counter])
			break;
		prune_graph(paths[counter], graph[0]);
		size = 0;
		counter++;
	}
	return (paths);
}
