/*
** EPITECH PROJECT, 2018
** lem_in
** File description:
** Removes path that are not relevant
*/

#include <stdlib.h>
#include <stdbool.h>
#include "graph.h"

static bool	path_too_long(int size_sum, int count, int nb_ants, int size)
{
	if (size_sum + count > nb_ants && size != count)
		return (true);
	return (false);
}

static int	do_pruning(graph_t ***paths, int *p_size, int nb_ants)
{
	int size_sum = 0;
	int count = 0;

	for (int i = 0; paths[i] != NULL; i++) {
		count = 0;
		while (paths[i][count]->id != ID_END)
			count++;
		if (i > 0 &&path_too_long(size_sum,count,nb_ants,p_size[i-1])) {
			p_size[i] = -1;
			return (0);
		}
		size_sum = size_sum + count;
		p_size[i] = count;
	}
	return (0);
}

int	*prune_path(graph_t ***paths, int nb_ants)
{
	int *paths_size = NULL;
	int count = 0;

	if (!paths)
		return (NULL);
	while (paths[count] != NULL)
		count++;
	paths_size = malloc(sizeof(int) * (count + 1));
	if (!paths_size)
		return (NULL);
	paths_size[count] = -1;
	do_pruning(paths, &paths_size[0], nb_ants);
	return (paths_size);
}

void	prune_active_path(int *p_size, int nb_ants)
{
	int size_sum = 0;
	int size = 0;

	for (int i = 0; p_size[i] != -1; i++) {
		size = p_size[i];
		if (i > 0 && path_too_long(size_sum,size,nb_ants,p_size[i-1])) {
			p_size[i] = -1;
			break;
		}
		size_sum = size_sum + size + 1;
	}
}
