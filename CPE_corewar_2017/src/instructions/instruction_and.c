/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** instruction 006
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "corewar.h"
#include "label.h"
#include "instruction.h"

int	instruction_and(int fd, char *str, label_t *label, int *pos)
{
	int number_func = AND;
	char **param = NULL;
	int nb = 0;
	int byte = 2;

	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	nb = calc_description(param);
	write(fd, &number_func, 1);
	write(fd, &nb, 1);
	byte += write_parameter(param[1], fd, label, *pos);
	byte += write_parameter(param[2], fd, label, *pos);
	byte += write_parameter(param[3], fd, label, *pos);
	free_parameter(param);
	*pos += byte;
	return (0);
}
