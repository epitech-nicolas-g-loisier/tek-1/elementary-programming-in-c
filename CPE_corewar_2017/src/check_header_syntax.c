/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** Check if the name and the comment of the champion are correct
*/

#include "check_file.h"
#include "error_list.h"
#include "my.h"

static int	check_str_syntax(char *str, int size)
{
	int len = 0;

	while (str[len] != '\0' && str[len] != '"')
		len++;
	if (str[len] != '"')
		return (E_SYNT);
	if (len > size)
		return (size == 2048 ? E_CLEN : E_NLEN);
	len++;
	while (str[len] != '\0') {
		if (str[len] != ' ' && str[len] != '\t')
			return (E_SYNT);
		len++;
	}
	return (0);
}

static bool	is_line_name(char *name, int *index, int *status)
{
	if (my_strncmp(".name", &name[*index], 5) != 0) {
		*status = E_NAME;
		return (false);
	}
	*index = *index + 5;
	while (name[*index] != '\"' && name[*index] != '\0') {
		if (name[*index] != ' ' && name[*index] != '\t') {
			*status = E_SYNT;
			return (false);
		}
		*index = *index + 1;
	}
	if (name[*index] == '\0' || name[(*index + 1)] == '\0') {
		*status = E_NAME;
		return (false);
	}
	*index = *index + 1;
	return (true);
}

int	check_name_error(char *name)
{
	static bool name_is_done = false;
	int idx = 0;
	int status = 0;

	if (name_is_done == true)
		return (0);
	while (name[idx] == ' ' || name[idx] == '\t')
		idx++;
	if (is_line_name(name, &idx, &status) == false)
		return (status);
	else {
		status = check_str_syntax(&name[idx], 128);
		if (status != 0)
			return (status);
	}
	name_is_done = true;
	return (0);
}

static bool	is_line_comment(char *comment, int *index, int *status)
{
	if (my_strncmp(".comment", &comment[*index], 8) != 0) {
		*status = E_CMNT;
		return (false);
	}
	*index = *index + 8;
	while (comment[*index] != '\"' && comment[*index] != '\0') {
		if (comment[*index] != ' ' && comment[*index] != '\t') {
			*status = E_SYNT;
			return (false);
		}
		*index = *index + 1;
	}
	if (comment[*index] == '\0' || comment[(*index + 1)] == '\0') {
		*status = E_CMNT;
		return (false);
	}
	*index = *index + 1;
	return (true);
}

int	check_cmnt_error(char *cmnt, bool name_is_defined)
{
	static bool comment_is_done = false;
	int idx = 0;
	int status = 0;

	if (comment_is_done == true || name_is_defined == false)
		return (0);
	while (cmnt[idx] == ' ' || cmnt[idx] == '\t')
		idx++;
	if (is_line_comment(cmnt, &idx, &status) == false)
		return (status);
	else {
		status = check_str_syntax(&cmnt[idx], 2048);
		if (status != 0)
			return (status);
	}
	comment_is_done = true;
	return (0);
}
