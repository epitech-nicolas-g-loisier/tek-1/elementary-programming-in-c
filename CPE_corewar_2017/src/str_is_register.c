/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Checks if the given arg is a register
*/

#include "error_list.h"
#include "my.h"

int	str_is_register(char *param, int *progress)
{
	int nbr = 0;

	if (!param || param[0] != 'r')
		return (E_INARG);
	if (my_str_isnum(&param[1])) {
		nbr = my_getnbr(&param[1]);
		if (nbr > 0 && nbr <= 16) {
			*progress = *progress + 1;
			return (0);
		}
		return (E_INREG);
	}
	return (E_INARG);
}
