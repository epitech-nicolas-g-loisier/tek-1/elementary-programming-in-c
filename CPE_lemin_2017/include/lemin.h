/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** Header for prototype
*/

#include <stdbool.h>
#include "graph.h"

#ifndef LEMIN_H_
#define LEMIN_H_

/* Prototypes for parsing file*/
int	check_end(char*);
int	check_link_syntax(char*, graph_t**);
int     check_room_syntax(char*);
int	check_if_linked(graph_t **);
int	create_link(graph_t*, graph_t*);
int     get_room_link(char*, graph_t**, int*);
graph_t	**get_room_pointer(char*, int, graph_t**);
char    *get_room_name(char*, int*);
graph_t **get_file_data(int *);
int	get_id(char *);
graph_t	***find_max_path(graph_t **);
int	*prune_path(graph_t ***, int);
void	free_graph(graph_t **);
void	free_path(graph_t ***);
graph_t	**get_path(graph_t **, int);
int	get_max_path(graph_t **);
int     add_ant_to_buffer(graph_t ***, int*);
int     put_ant(int , char *);
int     print_ant(graph_t ***, int *, int);
void	prune_active_path(int *, int);
bool	move_ants(graph_t **);
void	add_ants(graph_t ***, int *, int *);
void	init_room_id(graph_t ***, int *);

#endif /* LEMIN_H_ */
