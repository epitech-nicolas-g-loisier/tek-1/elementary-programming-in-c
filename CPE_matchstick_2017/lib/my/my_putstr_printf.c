/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Putstr with va_list in arg
*/

#include "my.h"
#include <unistd.h>

int	my_putstr_p(va_list ap)
{
	char	*str = va_arg(ap, char*);

	write(1, str, my_strlen(str));
	return (0);
}
