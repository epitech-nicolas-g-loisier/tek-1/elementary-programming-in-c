/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Checks if the label already exists
*/

#include <stdlib.h>
#include <stdbool.h>
#include "my.h"
#include "label.h"
#include "error_list.h"
#include <stdio.h>

static bool	label_already_exist(char *line, label_t *label)
{
	int len = 0;

	while (line[0] == ' ' || line[0] == '\t')
		line++;
	while (line[len] != ':' && line[len] != '\0')
		len++;
	while (label) {
		if (len == 0 || my_strncmp(line, label->name, len - 1) == 0)
			return (true);
		label = label->next;
	}
	return (false);
}

static label_t	*create_label(char *line, int pos)
{
	label_t *label = NULL;
	char *name = NULL;
	int len = 0;

	while (line[len] == ' ' || line[len] == '\t')
		line++;
	while (line[len] != ':')
		len++;
	name = malloc(sizeof(char) * len + 1);
	label = malloc(sizeof(label_t));
	if (name == NULL || label == NULL)
		return (NULL);
	name = my_strncpy(name, line, len);
	label->name = name;
	label->pos = pos;
	label->next = NULL;
	return (label);
}

int	check_label(char *line, label_t *label, int pos, int error)
{
	label_t *new_label = NULL;

	if (error > 0 && error <= E_MAX_ID)
		return (error);
	else if (label_already_exist(line, label->next))
		return (E_MLABL);
	new_label = create_label(line, pos);
	if (!new_label)
		return (E_MEM);
	while (label->next != NULL)
		label = label->next;
	label->next = new_label;
	return (0);
}
