/*
** EPITECH PROJECT, 2018
** Matchstick
** File description:
** The Matchstick game
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "matchstick.h"

int	take_number(char *str)
{
	int nb = 0;
	int idx = 0;

	while (str[idx]) {
		if (str[idx] < '0' && str[idx] > '9'){
			return (0);
		}
		else {
			nb = nb * 10;
			nb += str[idx] - '0';
		}
		idx++;
	}
	return (nb);
}

int	*get_max_matches(int lines, int max)
{
	int *matches = malloc(sizeof(int) * (lines + 2));
	int idx = 1;

	matches[0] = max;
	while (idx <= lines) {
		matches[idx] = idx * 2 - 1;
		idx++;
	}
	matches[idx] = -1;
	return (matches);
}

int	matchstick(char *lines, char *max)
{
	int nb_lines = take_number(lines);
	int *nb_max = get_max_matches(nb_lines, take_number(max));
	int win = 0;

	if (nb_max == 0 || nb_lines < 2 || nb_lines > 99) {
		write(2, "Error: the number of matches ", 29);
		write(2, "must be between 2 and 99\n", 25);
		return (84);
	}
	else {
		win = game_function(nb_lines, nb_max);
	}
	free(nb_max);
	return (win);
}

int	main(int argc, char **argv)
{
	int ret = 0;

	if (argc != 3) {
		write(2, "error: ./machstick take two arguments\n", 38);
		return (84);
	}
	ret = matchstick(argv[1], argv[2]);
	if (ret == 1)
		my_printf("I lost... snif... but I'll get you next time!!\n");
	else if (ret == 2)
		my_printf("You lost, too bad...\n");
	return (ret);
}
