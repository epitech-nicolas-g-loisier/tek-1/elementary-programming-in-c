/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** function for the lfork instruction
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"
#include "label.h"

int	instruction_lfork(int fd, char *str, label_t *label, int *pos)
{
	int instruct = LFORK;
	char **param;
	int octet = 1;
	int tmp = 0;

	write(fd, &instruct, 1);
	param = parameter_parser(str);
	if (param == NULL)
		return (84);
	tmp = direct_to_indirect(param[1]);
	octet += write_parameter(&param[1][tmp], fd, label, *pos);
	free_parameter(param);
	*pos += octet;
	return (0);
}
