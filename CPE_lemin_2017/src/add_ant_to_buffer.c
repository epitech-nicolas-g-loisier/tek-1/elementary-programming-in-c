/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** put ant to buffer
*/

#include <stdlib.h>
#include "graph.h"
#include "lemin.h"

int	add_ant_to_buffer(graph_t ***path, int *size)
{
	char *name = NULL;

	if (path == NULL || size == NULL) {
		return (84);
	}
	for (int y = 0; path[y] != NULL; y++) {
		for (int x = 0; path[y][(x + 1)] != NULL; x++) {
			if (path[y][x]->id == ID_ISU) {
				name = path[y][(x + 1)]->name;
				put_ant(path[y][x]->ant_id, name);
			}
		}
	}
	return (0);
}
