/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** if sti have good argument
*/

#include <stdio.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"
#include "error_list.h"

static int	err_arg_array(char **argv)
{
	int argc = 0;

	if (!argv)
		return (E_MEM);
	argc = check_length(3, argv);
	if (argc != 0) {
		free_parameter(argv);
		return (argc);
	}
	return (0);
}

static int	check_sti_arg(char *stock, int *pos)
{
	if (str_is_index(stock, pos)) {
		return (0);
	}
	return (str_is_register(stock, pos));
}

int check_sti(char *line, int *pos)
{
	char **stock = NULL;
	int i = 2;
	int status = 0;

	*pos = *pos + 2;
	stock = parameter_parser(line);
	status = err_arg_array(stock);
	if (status != 0)
		return (err_arg_array(stock));
	else if (str_is_register(stock[1], pos) != 0)
		return (E_INARG);
	while (i <= 3) {
		if (check_sti_arg(stock[i], pos) != 0) {
			free_parameter(stock);
			return (E_INARG);
		}
		i += 1;
	}
	free_parameter(stock);
	return (0);
}
