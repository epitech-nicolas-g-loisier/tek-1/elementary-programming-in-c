/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Intructions used in corewar
*/

#ifndef INSTRUCTION_
#	define INSTRUCTION_

enum instruction_code
{
	LIVE = 1,
	LD = 2,
	ST = 3,
	ADD = 4,
	SUB = 5,
	AND = 6,
	OR = 7,
	XOR = 8,
	ZJMP = 9,
	LDI = 10,
	STI = 11,
	FORK = 12,
	LLD = 13,
	LLDI = 14,
	LFORK = 15,
	AFF = 16,
	MAX_CODE = 17
};

#endif //INSTRUCTION_
