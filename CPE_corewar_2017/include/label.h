/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Label structure for corewar
*/

#ifndef LABEL_
#	define LABEL_

typedef struct label
{
	char *name;
	int pos;
	struct label *next;
} label_t;

#endif //LABEL_
