/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** if ld and lld have good argument given
*/

#include <stdio.h>
#include "my.h"
#include "corewar.h"
#include "instruction.h"
#include "error_list.h"

int	check_ld(char *line, int *pos)
{
	char **stock = NULL;
	const int arg = 2;

	*pos = *pos + 2;
	stock = parameter_parser(line);
	if (stock == NULL)
		return (E_MEM);
	else if (check_length(arg, stock) != 0)
		return (check_length(arg, stock));
	if (!str_is_indirect(stock[1], pos) && !str_is_direct(stock[1], pos)) {
		return (E_INARG);
	} else if (str_is_register(stock[2] , pos)) {
		return (E_INARG);
	}
	free_parameter(stock);
	return (0);
}
