/*
** EPITECH PROJECT, 2018
** Bootstrap Get Next Line
** File description:
** reads and returns the next n available characters
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

char	*read_next_n_bytes(int fd, int n)
{
	char	*buffer = malloc(sizeof(char) * n + 1);

	read(fd, buffer, n);
	buffer[n] = '\0';
	return buffer;
}
