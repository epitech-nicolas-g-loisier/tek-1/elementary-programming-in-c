/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** check if zjmp, fork, lfork have good argument
*/

#include <stdlib.h>
#include "corewar.h"
#include "error_list.h"

int	check_zjmp(char *str, int *pos)
{
	char **param = parameter_parser(str);
	int ret = 0;

	*pos = *pos + 1;
	if (param == NULL)
		return (E_MEM);
	else if (check_length(1, param) != 0) {
		ret = check_length(1, param);
		free_parameter(param);
		return (ret);
	}
	if (!str_is_index(param[1], pos))
		ret = E_INARG;
	free_parameter(param);
	return (ret);
}
