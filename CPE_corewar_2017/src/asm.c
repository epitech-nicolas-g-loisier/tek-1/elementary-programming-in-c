/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Convert assembler to binary
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "label.h"
#include "corewar.h"
#include "get_next_line.h"
#include "my.h"

char	*get_instruct_name(int index)
{
	static const char *instruction[32] = {"live ", "ld ", "st ", "add ",
					"sub ", "and ", "or ", "xor ", "zjmp ",
					"ldi ", "sti ", "fork ", "lld ",
					"lldi ", "lfork ", "aff ",
					"live\t", "ld\t", "st\t", "add\t",
					"sub\t", "and\t", "or\t", "xor\t",
					"zjmp\t", "ldi\t", "sti\t", "fork\t",
					"lld\t", "lldi\t", "lfork\t", "aff\t"};

	return ((char *)instruction[index]);
}

static int	write_instruction(int *fd, char *line, label_t *label,
				int *progress)
{
	if (is_short_function_index(&fd[1]))
		return (get_short_function_pointer(fd[1])(*fd, line, progress));
	return (get_long_function_pointer(fd[1])(*fd, line, label, progress));
}

static int	write_line(int fd, char *line, label_t *label, int *progress)
{
	int count = 0;
	int i = ignore_indent(line);

	while (count < 32 && my_strncmp(&line[i], get_instruct_name(count),
				my_strlen(get_instruct_name(count))) != 0)
		count++;
	if (count == 32) {
		count = 0;
		while (line[count] != ':' && line[count] != '\0')
			count++;
		if (line[count] != '\0')
			count++;
		while (line[count] == ' ' || line[count] == '\t')
			count++;
		if (line[count] != '\0')
			return (write_line(fd, &line[count], label, progress));
	} else
		return (write_instruction((int [2]){fd, count % 16 + 1},
					&line[i], label, progress));
	return (0);
}

static int	write_champion(int fd[2], char *line, label_t *label)
{
	int status = 0;
	int progress = 0;

	while (line) {
		if (line[0] != '\0' && line[0] != '#')
			status = write_line(fd[1], line, label, &progress);
		free(line);
		if (status == 84)
			return (84);
		line = get_next_line(fd[0]);
	}
	return (0);
}

int	build_champion(char *src_name, label_t *label, int size)
{
	int src_fd = open(src_name, O_RDONLY);
	int dest_fd = 0;
	char *line = NULL;

	if (src_fd == -1) {
		write(2, "asm: the file could not be opened\n", 34);
		return (84);
	}
	dest_fd = create_binary(src_name);
	if (dest_fd == -1 || init_binary(src_fd, dest_fd, &line, size) == 84)
		return (84);
	return (write_champion((int[2]){src_fd, dest_fd}, line, label));
}
