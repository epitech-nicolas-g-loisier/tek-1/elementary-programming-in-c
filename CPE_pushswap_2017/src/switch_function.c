/*
** EPITECH PROJECT, 2017
** Push Swap
** File description:
** Switch element between to list
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "pushswap.h"

/* 
** take the first element from lb
** and move it to the first position
** on the la list
*/
void	switch_listb_to_lista(list_t *list_b, list_t *list_a)
{
	add_to_list(list_a, list_b->next->value);
	remove_to_list(list_b->next);
	my_putstr("pa");
}

/* 
** take the first element from la
** and move it to the first position
** on the lb list
*/
void	switch_lista_to_listb(list_t *list_a, list_t *list_b)
{
	add_to_list(list_b, list_a->next->value);
	remove_to_list(list_a->next);
	my_putstr("pb");
}
