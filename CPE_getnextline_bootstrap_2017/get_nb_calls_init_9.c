/*
** EPITECH PROJECT, 2018
** Bootstrap Get Next Line
** File description:
** returns the number of times it was called, start at 9.
*/

int	get_nb_calls_init_9(void)
{
	static int count = 9;
	count++;
	return (count);
}
