/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Main function of the assembler
*/

#include <stdlib.h>
#include <unistd.h>
#include "label.h"
#include "corewar.h"

static void	free_label(label_t *label)
{
	label_t *temp = label;

	while (temp) {
		label = temp;
		temp = label->next;
		free(label->name);
		free(label);
	}
}

int	main(int argc, char **argv)
{
	label_t label = {"", 0, NULL};
	char *usage = "Usage: ./asm file_name[.s] ....\n";
	int size = 0;

	if (argc == 1) {
		write(2, usage, 32);
		return (84);
	}
	for (int i = 1; i < argc; i++) {
		if (check_file_content(argv[i], &label, &size) == 0
			&& check_used_label(argv[i], label.next))
			build_champion(argv[i], label.next, size);
		else
			return (84);
		free_label(label.next);
		label.next = NULL;
	}
	return (0);
}
