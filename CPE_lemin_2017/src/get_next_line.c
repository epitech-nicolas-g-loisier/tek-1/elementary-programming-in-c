/*
** EPITECH PROJECT, 2018
** Get Next Line
** File description:
** take the next line of a file
*/

#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include "get_next_line.h"

int	count_char(char *str, char end)
{
	int count = 0;

	if (str == NULL)
		return (-1);
	while (str[count] != '\0' && str[count] != end)
		count++;
	return (count);
}

int	check_end_line(char *str)
{
	int idx = 0;

	if (str == NULL)
		return (84);
	while (str[idx] != '\0') {
		if (str[idx] == '\n')
			return (1);
		idx++;
	}
	return (0);
}

char	*add_str_realloc(char *src1, char *src2, int mode)
{
	int length = count_char(src1, '\0') + count_char(src2, '\0');
	char *dest = malloc(sizeof(char) * length + 1);
	int idx = 0;

	if (dest == NULL || src1 == NULL || src2 == NULL)
		return (NULL);
	else if (mode == 1) {
		free(dest);
		free(src1);
		return (NULL);
	}
	for (idx = 0; src1[idx] != '\0'; idx++)
		dest[idx] = src1[idx];
	for (int tmp = 0; src2[tmp] != '\0'; tmp++) {
		dest[idx] = src2[tmp];
		idx++;
	}
	dest[idx] = '\0';
	free(src1);
	return (dest);
}

char	*return_line(char *str)
{
	int idx = 0;
	int tmp = 0;
	char *ret = malloc(sizeof(char) * count_char(str, '\n') + 1);

	if (ret == NULL || str == NULL)
		return (NULL);
	for (idx = idx; str[idx] != '\n' && str[idx] != '\0'; idx++)
		ret[idx] = str[idx];
	ret[idx] = '\0';
	if (str[idx] == '\0' && ret[0] == 0) {
		free(str);
		return (NULL);
	}
	idx++;
	for (idx = idx; str[(idx - 1)] != '\0'; idx++) {
		str[tmp] = str[idx];
		tmp++;
	}
	str[tmp] = '\0';
	return (ret);
}

char	*get_next_line(int fd)
{
	static char* store = NULL;
	char *buffer = malloc(sizeof(char) * (READ_SIZE + 1));
	int size = READ_SIZE;

	if (fd < 0 || READ_SIZE < 1 || READ_SIZE > INT_MAX || buffer == NULL)
		return (NULL);
	else if (store == NULL) {
		store = malloc(sizeof(char) * 1);
		store[0] = '\0';
	}
	while (size == READ_SIZE && check_end_line(store) == 0) {
		size = read(fd, buffer, READ_SIZE);
		buffer[size] = '\0';
		store = add_str_realloc(store, buffer, 0);
	}
	free(buffer);
	if (count_char(store, '\0') == 0)
		return (add_str_realloc(store, "\0", 1));
	buffer = return_line(&store[0]);
	return (buffer);
}
