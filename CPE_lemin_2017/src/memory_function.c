/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** Free memory for lemin
*/

#include <stdlib.h>
#include "graph.h"

void free_rooms(graph_t **rooms)
{
	for (int idx = 0; rooms[idx] != NULL; idx++) {
		free(rooms[idx]->name);
		free(rooms[idx]);
	}
	free(rooms);
}
