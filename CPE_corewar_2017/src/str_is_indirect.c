/*
** EPITECH PROJECT, 2018
** corewar
** File description:
** Checks if the arg given is an indirect
*/

#include <stdbool.h>
#include "my.h"
#include "corewar.h"

bool	str_is_indirect(char *param, int *progress)
{
	if (my_str_isnum(param) || str_is_label(param)) {
		*progress = *progress + 2;
		return (true);
	}
	return (false);
}
