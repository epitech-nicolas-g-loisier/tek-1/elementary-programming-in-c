/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** check if label is defined
*/

#include <stdbool.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"
#include "label.h"
#include "corewar.h"
#include "my.h"

static int	cmp_str_label(char *line, label_t *label)
{
	int idx = 0;
	int check = 0;

	while (label != NULL) {
		idx = 0;
		while (label->name[idx] != '\0') {
			if (line[idx] != label->name[idx]) {
				check = 1;
				break;
			}
			idx++;
		}
		if (check_para_separator(line[idx]) == 1 && check == 0)
			break;
		check = 0;
		label = label->next;
	}
	if (label == NULL)
		return (1);
	return (0);
}

static int	check_if_label_exist(char *line, label_t *label)
{
	int idx = 0;
	int ret = 0;

	if (line[1] == '\t' || line[1] == ' ')
		return (0);
	else {
		while (check_para_separator(line[idx]) == 0) {
			idx++;
		}
		ret = cmp_str_label(&line[1], label);
	}
	return (ret);
}

static int	check_if_label(char *line, label_t *label)
{
	int ret = 0;
	int tmp = 0;

	for (int idx = 0; line[idx] != '\0'; idx++) {
		tmp = check_para_separator(line[(idx+1)]);
		if (line[idx] == ':' && tmp == 0)
			ret = check_if_label_exist(&line[idx], label);
		if (ret != 0)
			return (ret);
	}
	return (ret);
}

static bool	check_label_error(int check, int line, char *name)
{
	if (check != 0) {
		write(2, "asm, ", 5);
		write(2, name, my_strlen(name));
		write(2, ", line ", 7);
		my_put_nbr(line);
		write(2, ": Undefined label.\n", 19);
		return (false);
	} else
		return (true);
}

bool	check_used_label(char *file, label_t *label)
{
	int fd = open(file, O_RDONLY);
	int check = 0;
	int count = -1;
	char *line = NULL;

	if (fd == -1) {
		return (false);
	} else {
		line = get_next_line(fd);
	}
	while (line != NULL && check == 0) {
		if (line[0] != '#' && line[0] != '.' && line[0] != '\0')
			check = check_if_label(line, label);
		free(line);
		line = get_next_line(fd);
		count++;
	}
	return (check_label_error(check, count, file));
}
