/*
** EPITECH PROJECT, 2018
** Lemin
** File description:
** check the syntaxe of room name declaration
*/

#include "lemin.h"

int	char_is_alpha_num(char c)
{
	if ((c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
		return (1);
	else if ((c >= 'a' && c <= 'z') || c == '_')
		return (1);
	else
		return (0);
}

int	check_room_name(char *str, int *idx)
{
	while (str[*idx] != ' ' && str[*idx] != '\t' && str[*idx] != '\0') {
		if (char_is_alpha_num(str[*idx]) == 0)
			return (84);
		*idx += 1;
	}
	if (str[*idx] == '\0')
		return (84);
	else {
		while (str[*idx] == ' ' || str[*idx] == '\t')
			*idx += 1;
	}
	if (str[*idx] == '\0')
		return (84);
	return (0);
}

int	check_room_pos(char *str, int *idx)
{
	int length = 0;

	while (str[*idx] != ' ' && str[*idx] != '\t' && str[*idx] != '\0') {
		if ((str[*idx] < '0' || str[*idx] > '9') || length > 12)
			return (84);
		*idx += 1;
		length += 1;
	}
	while ((str[*idx] == ' ' || str[*idx] == '\t') && str[*idx] != '\0')
		*idx += 1;
	if (str[*idx] == '\0')
		return (84);
	length = 0;
	while (str[*idx] != ' ' && str[*idx] != '\t' && str[*idx] != '\0') {
		if ((str[*idx] < '0' || str[*idx] > '9') || length > 12)
			return (84);
		*idx += 1;
		length += 1;
	}
	return (0);
}

int	check_room_syntax(char *str)
{
	int idx = 0;
	int ret = 0;

	while (str[idx] == ' ' || str[idx] == '\t')
		idx++;
	if (check_room_name(str, &idx) == 84)
		return (84);
	else if (check_room_pos(str, &idx) == 84)
		return (84);
	else
		ret = check_end(&str[idx]);
	str[idx] = '\0';
	return (ret);
}
