/*
** EPITECH PROJECT, 2018
** need4stek
** File description:
** function which get answer
*/

#include <stdlib.h>
#include "get_next_line.h"
#include "n4s.h"

void	get_answer(char **line, int **parsing, int value)
{
	free(*line);
	free(*parsing);
	*line = get_next_line(0);
	*parsing = line_parser(*line, value);
}