/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_putstr.c
*/

#include "my.h"

int	my_putstr(char *str)
{
	int	i = 0;

	while (str[i] != 0){
		my_putchar(str[i]);
		i = i + 1;
	}
	return (0);
}
