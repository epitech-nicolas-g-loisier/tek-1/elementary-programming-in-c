/*
** EPITECH PROJECT, 2017
** BSQ
** File description:
** find the largest possible square
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <bsq.h>
#include <my.h>

void	find_square(int x, int y, char **map, square_t *square)
{
	int	tmp_y = y;
	int	tmp_x = x;
	int	tmp_size = 0;

	while (map[tmp_x] != NULL && map[tmp_x][y] != 'o'){
		tmp_y = y;
		while (map[tmp_x][tmp_y] != 'o' && map[tmp_x][tmp_y] != '\n'){
			tmp_y++;
		}
		if ((tmp_x - x) == tmp_size && tmp_size != 0)
			break;
		else if (tmp_size == 0 || tmp_size > (tmp_y - y))
			tmp_size = (tmp_y - y);
		tmp_x++;
	}
	if (tmp_size > (tmp_x - x))
		tmp_size = (tmp_x - x);
	if (tmp_size > square->size){
		square->size = tmp_size;
		square->x = x;
		square->y = y;
	}
}

void	modify_map(char **map, square_t square)
{
	int	i = square.x;
	int	j = square.y;
	int	count = 0;

	while (j > 0 && map[i] != NULL && i <= (square.size + square.x)
		&& map[i][(j - 1)] != 'o'){
		count++;
		i++;
	}
	i = square.x;
	if (count == square.size + 1 && j > 0) {
		j--;
		square.y --;
	}
	while (i < (square.size + square.x) && map[i][j] != 0){
		j = square.y;
		while (j < (square.size + square.y)){
			map[i][j] = 'x';
			j++;
		}
		i++;
	}
}

void	display_result(char **map)
{
	int	i = 0;

	while (map[i] != NULL){
		write(1, map[i], my_strlen(map[i]));
		i++;
	}
}

void	find_largest_square(char const *filepath)
{
	int	nb_line = get_nbr_from_first_line(filepath);
	int	length_line = get_length_of_first_line(filepath);
	char	**map = take_map(nb_line, length_line, filepath);
	square_t square;
	int	i = 0;
	int	j = 0;

	square.size = 0;
	while (map[i] != NULL){
		j = 0;
		while (map[i][j] != '\n'){
			if (square.size <= (nb_line - i))
				find_square(i, j, map, &square);
			j++;
		}
		i++;
	}
	if (square.size != 0)
		modify_map(map, square);
	display_result(map);
}
