/*
** EPITECH PROJECT, 2018
** Corewar Assembler
** File description:
** check if and, or, xor have good argument
*/

#include <stdlib.h>
#include "corewar.h"
#include "error_list.h"

static int	free_parsing(char **argv, int status)
{
	free_parameter(argv);
	return (status);
}

static int	check_is_arg_register(char **argv, int *pos)
{
	int status = str_is_register(argv[3], pos);

	free_parameter(argv);
	return (status);
}

int	check_and(char *str, int *pos)
{
	char **param = parameter_parser(str);
	int ret = 0;

	*pos = *pos + 2;
	if (param == NULL)
		return (E_MEM);
	else if (check_length(3, param) != 0) {
		ret = check_length(3, param);
		return (free_parsing(param, ret));
	}
	ret = str_is_param(param[1], pos);
	if (ret != 0) {
		free_parameter(param);
		return (ret);
	} else if (str_is_param(param[2], pos) != 0) {
		ret = str_is_param(param[2], pos);
		free_parameter(param);
		return (ret);
	}
	return (check_is_arg_register(param, pos));
}
