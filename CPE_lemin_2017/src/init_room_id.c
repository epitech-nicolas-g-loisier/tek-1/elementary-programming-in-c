/*
** EPITECH PROJECT, 2018
** lem_in
** File description:
** function which initialise room_id to ID_N0R if it's value is ID_ISU
*/

#include <stdlib.h>
#include "lemin.h"
#include "graph.h"

void	init_room_id(graph_t ***paths, int *paths_size)
{
	int paths_idx = 0;
	int room_idx = 0;

	while (paths[paths_idx] != NULL) {
		while (room_idx < paths_size[paths_idx]) {
			if (paths[paths_idx][room_idx]->id == ID_ISU)
				paths[paths_idx][room_idx]->id = ID_NOR;
			room_idx ++;
		}
		paths_idx ++;
	}
}
